# Valencia

*Valencia* is designed to simulate valence change memory cells, i.e., nanoelectronic devices that might spark a renaissance of analog computation and data storage. 

## Building the core library

On Windows, [MSYS2](https://www.msys2.org/) is the recommended build platform. In the `UCRT64` environment, install `mingw-w64 GCC`, `mingw-w64 CMake` and `mingw-w64 Ninja`:

```
pacman -S mingw-w64-ucrt-x86_64-gcc
pacman -S mingw-w64-ucrt-x86_64-cmake
pacman -S mingw-w64-x86_64-ninja
```

The core library is built using CMake. If your build environment is set up, building `libvalencia_core.a` reduces to:

```
cd core
mkdir build
cmake -B build -G Ninja
cmake --build build
```

## Building the Python bindings

Besides building *Valencia* as a static C++ library, you can also build it as a Python library. To do so, download the latest version of [pybind11](https://github.com/pybind/pybind11) and unzip its source files in `python/thirdparty/` (or adjust the corresponding path in `python/CMakeLists.txt`).

Building `valencia_python` is similiar to building `valencia_core`:

```
cd python
mkdir build
cmake -B build -G Ninja
cmake --build build
```

If the build process succeeds, you will find a file `valencia.<python version>-<architecure>.pyd` in the `python/build/` directory. Bring up a Python shell to confirm that building `valencia_python` was successfull:

```
cd build
python
import valencia
```

If no error message appears: Congratulations! You are now ready to work with *Valencia* in Python. Exemplary Python scripts follow soon.

## License
*Valencia* is licensed under the GNU General Public License, Version 3.

`valencia_python` uses Wenzel Jakob's [pybind11](https://github.com/pybind/pybind11), released under a BSD-style license.