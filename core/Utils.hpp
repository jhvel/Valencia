#ifndef STRING_HELPER_H
#define STRING_HELPER_H

// #include <vector>
#include <stdexcept>
#include <cmath>

namespace Valencia {
    
    struct RuntimeError : public std::runtime_error {
        template<typename... Args>
        RuntimeError(const char * format, Args... args) : 
            std::runtime_error(StringBuffer(format, args...).getString()) {}
    };

    namespace StringHelper {
        // Legacy functions (okay)
        // Convert an integer to superscript text
        // const char * toSuperscript(const int number);
        // Convert an exponent to metric prefix, eg. exp = 3 (10^3) -> 'k' (kilo-)
        const char * toPrefix(const int exponent);
        
        // double stringToDouble(const char * string);
        double readDouble(const char * buffer);
    };

    namespace Utils {
        int sign(const double & value);
        int toStride3Exponent(const int exponent);
        int getExponent(const double & value, int stride = 1);
        
        // Source: https://stackoverflow.com/questions/466204/rounding-up-to-next-power-of-2
        template<typename U>
        U bitCeil(U value) {
            static_assert(std::is_unsigned<U>::value, "Only unsigned types supported");
            value--;
            for (std::size_t i = 1; i < sizeof(value) * CHAR_BIT; i *= 2) {
                value |= value >> i;
            }
            return ++value;
        }
        
        template<typename T>
        T & max (const T & value1, const T & value2) {
            return value1 > value2? value1 : value2;
        }
        
        template<typename T>
        T & min (const T & value1, const T & value2) {
            return value1 < value2? value1 : value2;
        }
        
        double interpolate(const double * data, const double & x);
        
        // data: discrete data points
        // Data callback: int -> double
        template<typename C>
        double interpolate(const double * data, const C & modifier, const double & x) {
            const int x0 { static_cast<int>(std::ceil(x)) };
            const int x1 { static_cast<int>(std::floor(x)) };
            
            if (x0 == x1)
                return modifier(x1, data[x1]);
            else {
                const double y0 { modifier(x0, data[x0]) };
                const double y1 { modifier(x1, data[x1]) };
                return (y0 * (x1 - x) + y1 * (x - x0)) / (x1 - x0);  
            }
        }
    };
        
        // bool contains(std::vector<const char *> & vector, const char * string);
        
    struct SimulationState {
        int i { 0 };
        double t { 0 };
        double dt { 0 };
        double V { 0 };
    };
    
} // namespace Valencia

#endif