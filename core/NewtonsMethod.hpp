#ifndef NEWTONS_METHOD_H
#define NEWTONS_METHOD_H

#include "Expression.hpp"

namespace Valencia {
    
    template<std::size_t M, std::size_t N>
    struct Matrix {
        Matrix(Array<double, M * N> & array);

        double & get(const std::size_t i, const std::size_t j);
        double & operator () (const std::size_t i, const std::size_t j) ;

        const double & get(const std::size_t i, const std::size_t j) const;
        const double & operator () (const std::size_t i, const std::size_t j) const;

        void swapRows(const std::size_t i1, const std::size_t i2);
        void swapColumns(const std::size_t j1, const std::size_t j2);
        
        double calcNorm() const;
        
        void print() const;

        // Syntactic sugar tailored to NonLinearSystem

        void operator *= (const double factor);
        Matrix<M, N> & operator - ();

        private:
            static constexpr int ROWS = 0;
            static constexpr int COLS = 1;

            const std::size_t size[2] { M, N };   // Size   in i, j
            const std::size_t stride[2] { N, 1 }; // Stride in i, j
            std::size_t map[2][M > N? M : N] {};  // Map to swap rows/columns
            
            Array<double, M * N> & data;
            
            inline std::size_t getDataIndex(const std::size_t & i, const std::size_t & j) const;
            void swap(const int l, const std::size_t k1, const std::size_t k2);
            
            void reset();
            double & getDirect(const std::size_t k);
            const double & getDirect(const std::size_t k) const;
    };

    template<std::size_t M>
    using Vector = Matrix<M, 1>;

    template<std::size_t M, std::size_t N>
    struct LinearSystem {
        static Vector<M> solve(Matrix<M, N> & matrix, Vector<M> & vector);
        
       
        static void solve(Array<double, M * N> & matrixIn, Array<double, M> & vectorIn, Array<double, M> & result);

        private:
            static bool findPivot(
                const Matrix<M, N> & matrix, const std::size_t iFrom, 
                std::size_t & pivotIndex
            );
    };
    

    template <typename... T>
    struct NonLinearSystem;
    
    // Implementation as template specialization
    template<typename... E, typename... A>
    struct NonLinearSystem<Expression::Expressions<E...>, 
                           Expression::Arguments<A...>>
    {
        static_assert(sizeof...(E) == sizeof...(A), 
            "Number of equations does not match the number of arguments");
        
        static constexpr std::size_t N { sizeof...(A) };
        using Vector = Array<double, N>;
        using Matrix = Array<double, N * N>;
        
        static constexpr bool DEBUG { false };
        
        NonLinearSystem(
            const Expression::Expressions<E...> & equations,
            const Expression::Arguments<A...> & arguments
        );
        
        // Throws std::runtime_error
        template<typename C>
        auto solve(
            const Vector & x0,
            const C callback
        );
        
        // Throws std::runtime_error
        auto solve(
            const Vector & x0,
            const double maxNorm,
            const double maxIterationCount
        );
        
        private:  
            Vector y, dx;
            Matrix J;
            
            using Jacobian = decltype(
                std::declval<Expression::Expressions<E...>>()
                    .derive(std::declval<Expression::Arguments<A...>>())
            );
            
            const Expression::Expressions<E...> f;
            Expression::VariableAdapter<A...> x;
            
            const Jacobian jacobian;
    };

    #include "NewtonsMethod.tpp"
    
} // namespace Valencia

#endif