#include "File.hpp"

#include <stdexcept>

using namespace Valencia;

/* ============ */
/* === File === */
/* ============ */

File::File(const char * fileName, Mode mode) : fileName(fileName), mode(mode) {
    // #ifdef _WIN32
    // if (tinyfd_winUtf8) {
        // file = _wfopen(
            // tinyfd_utf8to16(fileName), 
            // mode == Mode_Write? L"w" : L"r"
        // );
    // } else
    // #endif

    if (!fileName)
        throw std::runtime_error("No file name provided");
    
    const char * modeString = (mode == Mode_Write)? "w" : "r";
    file = std::fopen(fileName, modeString);
    
    if (!file)
        throw std::runtime_error("Could not open file");
}

File::~File() {
    if (file)
        std::fclose(file);
}

bool File::canWrite() const {
    return mode == Mode_Write;
}

/* ================== */
/* === FileBuffer === */
/* ================== */

FileBuffer::FileBuffer(
    File & outputFile,
    std::size_t initialSize,
    std::size_t maxSize
) : StringBuffer(initialSize, maxSize), outputFile(outputFile) {}

FileBuffer::~FileBuffer() {
    if (!hasError && !flushTo(outputFile))
        raise("FileBuffer destructor: Error while flushing file");
}

bool FileBuffer::grow() {
    return StringBuffer::grow() || flushTo(outputFile);
}

bool FileBuffer::flushTo(File & file) {
    if (file.canWrite()) {
        putTerminator(); // Just to be sure
        if (std::fputs(getString(), file) != EOF) {
            reset();
            return true;
        }
    }
    return false;
}
