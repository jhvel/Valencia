#include "Expression.hpp"

using namespace Valencia::Expression;

/* === Constant === */

Constant::Constant(double value) : value(value) {}

void Constant::print() const {
    printf_s("%e c", value);
}

/* === Parameter === */

Parameter::Parameter(double & value) : 
    value(&value) {}

// Required?
Parameter::Parameter(const Parameter & source) : 
    value(source.value) {}


void Parameter::setValue(const double value) {
    *this->value = value;
}

void Parameter::print() const {
    printf_s("p: %lf @ %p", *value, (void *) value);
}

/* === ParameterExpression === */

ParameterExpression::ParameterExpression(double & value) : 
    Expression<Parameter>(value) {}

void ParameterExpression::operator = (const double value) {
    this->expression.setValue(value);
}

/* == Zero == */

Zero::Zero() : Constant(0) {}

void Zero::print() const {
    printf_s("0z");
}

/* === One === */

One::One() : Constant(1) {}

void One::print() const {
    printf_s("1z");
}

/* === Error === */

Error::Error(const char * message) : message(message) {}

