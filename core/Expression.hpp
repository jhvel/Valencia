#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <cstdio>
#include <tuple>
#include <stdexcept>
#include <cmath>

#include "Util.hpp"

namespace Valencia { namespace Expression {

    // Forward declarations
    template<std::size_t I>
    struct Argument;
    
    template<std::size_t I>
    struct ArgumentExpression;
    
    template<typename... A>
    using Arguments = std::tuple<A...>;
    
    /* === Variable === */
    
    template<std::size_t I>
    struct Variable {
        Variable(const double value);
        double getValue() const;
        
        static constexpr std::size_t id { I };
        private:
            const double value;
    };
    
    template<typename... V>
    using Variables = std::tuple<V...>;
    
    template<int N, typename... T>
    using TypeOfItem = typename std::tuple_element<N, std::tuple<T...>>::type;

    template<int N, std::size_t I, typename... V>
    constexpr inline typename std::enable_if<N == sizeof...(V), int>::type 
    findVariableById() {
        return -1;
    }

    template<int N, std::size_t I, typename... V>
    constexpr inline typename std::enable_if<N < sizeof...(V), int>::type 
    findVariableById() {
        using T = TypeOfItem<N, V...>;
        return I == T::id? N : findVariableById<N + 1, I, V...>(); 
    }
    
    /* === ReferenceVariable === */
    
    template<std::size_t I>
    struct ReferenceVariable {
        ReferenceVariable(double * value);
        double getValue() const;
        
        static constexpr std::size_t id { I };
        private:
            const double * value;
    };
    
    /* === VariableAdapter === */
    
    template<typename... A>
    class VariableAdapter {
        template<std::size_t I>
        ReferenceVariable<I> convertArgument(
            const int index, const ArgumentExpression<I> &
        );
    
        template<std::size_t... N>
        auto convertArguments(
            const std::tuple<A...> & arguments, 
            const std::index_sequence<N...>
        );
        
        template<std::size_t... N>
        auto convertArguments(const std::tuple<A...> & arguments);
            
        public:
            VariableAdapter(const std::tuple<A...> & arguments);
                
            using Variables = decltype(
                std::declval<VariableAdapter<A...>>()
                    .convertArguments(std::declval<Arguments<A...>>())
            );
            
            // Add casting operators
            
            Array<double, sizeof...(A)> values;
            const Variables variables;
    };  
    
    /* === Expression === */

    // "Base class" of all Expressions
    template<typename T>
    struct Expression {
        Expression();
        Expression(const T & expression);
        
        template<typename... V>
        auto evaluate(const Variables<V...> & variables) const;
        
        template<std::size_t I>
        auto derive(const Argument<I> & argument) const;

        /* Operator functions as shorthands */
        
        // Evaluate
        template<typename... V>
        double operator () (const V&... variables) const;
        
        // Evaluate
        template<typename... V>
        double operator () (const Variables<V...> & variables) const;
        
        template<std::size_t I>
        auto derive(const ArgumentExpression<I> & other) const;

        // Negation
        auto operator - () const; // Does not really belong here

        void print() const;

        protected:
            T expression; // Do not store reference here!
    };
    
    // Forward declaration
    struct Zero;
    struct One;
    
    /* === NonArgument == */
    
    struct NonArgument {
        template<std::size_t I>
        Expression<Zero> derive(const Argument<I> &) const;
    };
    
    /* === Constant === */
    
    struct Constant : public NonArgument {
        Constant(double value);

        template<typename... V>
        double evaluate(const Variables<V...> &) const;
        
        void print() const;

        private:
            const double value;
    };
    
    /* === Parameter === */
     
    // TODO: Merge Parameter and Constant?

    struct Parameter : public NonArgument {
        Parameter(double & value);
        Parameter(const Parameter & source);
        
        template<typename... V>
        double evaluate(const Variables<V...> &) const;
        
        void setValue(const double value);
        
        void print() const;

        private:
            double * value;
    };
    
    /* === Error === */
    
    struct Error {
        Error(const char * message);
       
        template<typename... V>
        double evaluate(const Variables<V...> &) const;
        
        template<std::size_t I>
        Expression<Zero> derive(const Argument<I> &) const;
        
        private:
           const char * message;
    };

    /* === Argument === */ 

    template<std::size_t I>
    struct Argument {
        template<typename... V>
        double evaluate(const Variables<V...> & variables) const;
        
        template<std::size_t J>
        auto derive(const Argument<J> & argument) const;
        
        Variable<I> assignValue(const double value) const;
        
        void print() const;
    };
    
    /* === Zero and One === */
    
    // Special Constants used for template-based optimization
    // of expressions like "0 * ..." 

    struct Zero : public Constant {
        Zero();
        void print() const;
    };
    
    struct One : public Constant {
        One();
        void print() const;
    };
    
    /* === ArgumentExpression === */
  
    // Adds syntactic sugar for Expression<Argument>
    
    template<std::size_t I>
    struct ArgumentExpression : public Expression<Argument<I>> {
        Variable<I> operator = (const double value) const;
        const Argument<I> & getArgument() const;
    };
    
    /* === ParameterExpression === */
  
    // Adds syntactic sugar for Expression<Parameter>
    
    struct ParameterExpression : public Expression<Parameter> {
        ParameterExpression(double & value);
        void operator = (const double value);
    };
    
    /* === UnaryExpression === */

    template<typename I, typename O>
    struct UnaryExpression {
        UnaryExpression(const Expression<I> & inner);
        
        template<typename... V>
        double evaluate(const Variables<V...> & variables) const;

        template<std::size_t N>
        auto derive(const Argument<N> & argument) const;
        
        void print() const;

        private:
            const Expression<I> inner; // Do not store reference here!
    };

    /* === BinaryExpression === */

    template<typename L, typename R, typename O>
    struct BinaryExpression {
        BinaryExpression(
            const Expression<L> & left,
            const Expression<R> & right
        );
     
        template<typename... V>
        double evaluate(const Variables<V...> & variables) const;
        
        template<std::size_t I>
        auto derive(const Argument<I> & argument) const;
        
        void print() const;

        private:
            const Expression<L> left; // Do not store reference here!
            const Expression<R> right;
    };
    
    /* === ConditionalExpression === */
    
    template<typename C, typename L, typename R>
    struct ConditionalExpression {
        ConditionalExpression(
            const Expression<C> & condition, 
            const Expression<L> & left,
            const Expression<R> & right
        );
     
        template<typename... V>
        double evaluate(const Variables<V...> & variables) const;
        
        template<std::size_t I>
        auto derive(const Argument<I> & argument) const;
        
        void print() const;
      
        private:
            const Expression<C> condition;
            const Expression<L> left; // Do not store reference here!
            const Expression<R> right;
    };
    
    /* === Expressions === */

    template<typename... E>
    struct Expressions {
        Expressions(const std::tuple<E...> & expressions);
        
        template<typename... V>
        auto operator () (const Variables<V...> & variables) const;
        
        template<typename... V>
        auto operator () (
            const Variables<V...> & variables,
            Array<double, sizeof...(E)> & result
        ) const;

        template<typename... V>
        void evaluate(
            const Variables<V...> & variables, 
            Array<double, sizeof...(E)> & result
        ) const;
        
        template<typename... A>
        auto derive(const Arguments<A...> & arguments) const;
        
        private:
            const std::tuple<E...> expressions; // No reference
            
            // This helper function is required because we can not use 
            // decltype() to declare a parameter pack
            template<typename... F>
            auto wrap(const std::tuple<F...> & expressions) const;

            template<typename... A, std::size_t... I>
            auto deriveAllForEach(
                const std::index_sequence<I...>,
                const Arguments<A...> & arguments
            ) const;

            template<typename F, typename... A, std::size_t... I>
            auto deriveForEach(
                const F & expression,
                const Arguments<A...> & arguments, 
                const std::index_sequence<I...>
            ) const;
    };

    namespace Operator {   
        constexpr bool DEBUG { false };
        
        /* Unary Operators */

        #define DECLARE_UNARY_OPERATOR(NAME)                             \
        template <typename I>                                            \
        auto NAME(const Expression<I> & inner);                          \
                                                                         \
        struct NAME ## _operator {                                       \
            template<typename I, typename... V>                          \
            static double evaluate(                                      \
                const Expression<I> & inner, const Variables<V...> & v); \
                                                                         \
            template<typename I, std::size_t N>                          \
            static auto derive(                                          \
                const Expression<I> & inner, const Argument<N> & a);     \
                                                                         \
            template<typename I>                                         \
            static void print(const Expression<I> & inner) {             \
                printf_s( #NAME "("); inner.print();  printf_s(")");     \
            }                                                            \
        };                                                               \
                                                                         \
        template <typename I>                                            \
        inline auto NAME(const Expression<I> & inner) {                  \
            using T = UnaryExpression<Expression<I>, NAME ## _operator>; \
            return Expression<T>(T(inner));                              \
        }
       
        /* Binary Operators */
        
        #define DECLARE_BINARY_OPERATOR_NO_PRINT(NAME)                       \
        template <typename L, typename R>                                    \
        auto NAME(const Expression<L> & left, const Expression<R> & right);  \
                                                                             \
        struct NAME ## _operator {                                           \
            template<typename L, typename R, typename... V>                  \
            static double evaluate(                                          \
                const Expression<L> & left, const Expression<R> & right,     \
                const Variables<V...> & v                                    \
            );                                                               \
                                                                             \
            template<typename L, typename R, std::size_t I>                  \
            static auto derive(                                              \
                const Expression<L> & left, const Expression<R> & right,     \
                const Argument<I> & a                                        \
            );                                                               \
                                                                             \
            template<typename L, typename R>                                 \
            static void print(                                               \
                const Expression<L> & left, const Expression<R> & right      \
            );                                                               \
        };                                                                   \
                                                                             \
        template <typename L, typename R>                                    \
        inline auto NAME(                                                    \
            const Expression<L> & left, const Expression<R> & right          \
        ) {                                                                  \
            using T = BinaryExpression<                                      \
                Expression<L>, Expression<R>, NAME ## _operator>;            \
            return Expression<T>(T(left, right));                            \
        }                                                                    \
                                                                             \
        template <typename L>                                                \
        inline auto NAME(                                                    \
            const Expression<L> & left, const double right                   \
        ) {                                                                  \
            return NAME(left, Expression<Constant>(right));                  \
        }                                                                    \
                                                                             \
        template <typename R>                                                \
        inline auto NAME(                                                    \
            const double left, const Expression<R> & right                   \
        ) {                                                                  \
            return NAME(Expression<Constant>(left), right);                  \
        }
        
        
        #define DEFAULT_PRINT(NAME)                        \
        template<typename L, typename R>                   \
        void NAME ## _operator::print(                     \
            const Expression<L> & left,                    \
            const Expression<R> & right                    \
        ) {                                                \
            printf_s( #NAME "(");                          \
            left.print();  printf_s(", "); right.print();  \
            printf_s(")");                                 \
        }
        
        #define BASIC_PRINT(NAME, SYMBOL)              \
        template<typename L, typename R>               \
        void NAME ## _operator::print(                 \
            const Expression<L> & left,                \
            const Expression<R> & right                \
        ) {                                            \
            printf_s("(");                             \
            left.print();  printf_s(" "  #SYMBOL " "); \
            right.print();                             \
            printf_s(")");                             \
        }
        
        
        #define DECLARE_BINARY_OPERATOR(NAME)  \
        DECLARE_BINARY_OPERATOR_NO_PRINT(NAME) \
        DEFAULT_PRINT(NAME)
        
        
        #define DECLARE_BASIC_OPERATOR(NAME, SYMBOL)                \
        DECLARE_BINARY_OPERATOR_NO_PRINT(NAME)                      \
        BASIC_PRINT(NAME, SYMBOL)                                   \
                                                                    \
        template<typename L, typename R>                            \
        inline auto operator SYMBOL (                               \
            const Expression<L> & left, const Expression<R> & right \
        ) {                                                         \
            return NAME(left, right);                               \
        }                                                           \
                                                                    \
        template<typename L>                                        \
        inline auto operator SYMBOL (                               \
            const Expression<L> & left, const double right          \
        ) {                                                         \
            return NAME(left, right);                               \
        }                                                           \
                                                                    \
        template<typename R>                                        \
        inline auto operator SYMBOL (                               \
            const double left, const Expression<R> & right          \
        ) {                                                         \
            return NAME(left, right);                               \
        }
        
        // TODO: Merge constant expressions in general

        /* Optimizations */
         
        inline auto multiply(const Expression<Zero> &, const Expression<Zero> &) {
            return Expression<Zero>();      /* 0 * 0 = 0 */
        }
        
        template<typename R>
        inline auto multiply(const Expression<Zero> &, const Expression<R> &) {
            return Expression<Zero>();      /* 0 * x = 0 */
        }
        
        template<typename L>
        inline auto multiply(const Expression<L> &, const Expression<Zero> &) {
            return Expression<Zero>();      /* x * 0 = 0 */
        }
        
        
        template<typename R>
        inline auto multiply(const Expression<One> &, const Expression<One> &) {
            return Expression<One>();       /* 1 * 1 = 1 */
        }
        
        template<typename R>
        inline auto multiply(const Expression<One> &, const Expression<R> & right) {
            return right;                   /* 1 * x = x */
        }
        
        template<typename L>
        inline auto multiply(const Expression<L> & left, const Expression<One> &) {
            return left;                    /* x * 1 = x */
        }
     

        inline auto add(const Expression<Zero> &, const Expression<Zero> &) {
            return Expression<Zero>();      /* 0 + 0 = 0 */
        }
        
        template<typename R>
        inline auto add(const Expression<Zero> &, const Expression<R> & right) {
            return right;                   /* 0 + x = x */
        }
        
        template<typename L>
        inline auto add(const Expression<L> & left, const Expression<Zero> &) {
            return left;                    /* x + 0 = x */
        }
             
        inline auto add(const Expression<One> &, const Expression<One> &) {
            return Expression<Constant>(2); /* 1 + 1 = 2 */
        }
        
        
        inline auto subtract(const Expression<Zero> &, const Expression<Zero> &) {
            return Expression<Zero>();      /* 0 - 0 = 0 */
        }
        
        template<typename L>
        inline auto subtract(const Expression<L> & left, const Expression<Zero> &) {
            return left;                    /* x - 0 = x */
        }
        
        inline auto subtract(const Expression<One> &, const Expression<One> &) {
            return Expression<Zero>();      /* 1 - 1 = 0 */
        }
        
        
        template<typename R>
        inline auto divide(const Expression<Zero> &, const Expression<R> & right) {
            return Expression<Zero>();      /* 0 / x = 0 (?) */
        }
        
        template<typename L>
        inline auto divide(const Expression<L> & left, const Expression<One> &) {
            return left;                    /* x / 1 = x */
        }
           
              
        // Arithmetic operators
        DECLARE_BASIC_OPERATOR(add,      +)
        DECLARE_BASIC_OPERATOR(subtract, -)
        DECLARE_BASIC_OPERATOR(multiply, *)
        DECLARE_BASIC_OPERATOR(divide,   /)
        
        // Comparsion operators
        DECLARE_BASIC_OPERATOR(greater,      > )
        DECLARE_BASIC_OPERATOR(greaterEqual, >=)
        DECLARE_BASIC_OPERATOR(less,         < )
        DECLARE_BASIC_OPERATOR(lessEqual,    <=)
        DECLARE_BASIC_OPERATOR(equal,        ==)
        DECLARE_BASIC_OPERATOR(notEqual,     !=)
        
        // Unary functions
        DECLARE_UNARY_OPERATOR(exp )
        DECLARE_UNARY_OPERATOR(ln  )
        DECLARE_UNARY_OPERATOR(sqrt)
        // DECLARE_UNARY_OPERATOR(abs)
        
        // Trigonometric functions
        DECLARE_UNARY_OPERATOR(sin)
        DECLARE_UNARY_OPERATOR(cos)
        DECLARE_UNARY_OPERATOR(tan)

        // Inverse trigonometric functions
        DECLARE_UNARY_OPERATOR(asin)

        // Hyberpolic functions
        DECLARE_UNARY_OPERATOR(sinh)
        DECLARE_UNARY_OPERATOR(cosh)
        DECLARE_UNARY_OPERATOR(tanh)
  
        DECLARE_BINARY_OPERATOR(pow)
        DECLARE_BINARY_OPERATOR(min)
        DECLARE_BINARY_OPERATOR(max)
        
        // Conditional "operator"
        
        template<typename C, typename L>
        struct IfElseBuilder {
            IfElseBuilder(
                const Expression<C> & condition,
                const Expression<L> & left
            );
            
            template<typename R>
            inline auto else_(const Expression<R> & right);
            inline auto else_(const double right);
            
            private:
                const Expression<C> condition;
                const Expression<L> left;
        };
        
        template<typename C, typename L>
        auto if_(const Expression<C> & condition, const Expression<L> & left);
        
        template<typename C>
        auto if_(const Expression<C> & condition, const double left);
        
        template<typename C, typename L, typename R>
        auto ifElse(
            const Expression<C> & condition,
            const Expression<L> & left,
            const Expression<R> & right
        );
        
        template<typename C, typename L>
        auto ifElse(
            const Expression<C> & condition,
            const Expression<L> & left,
            const double right
        );
        
        template<typename C, typename R>
        auto ifElse(
            const Expression<C> & condition,
            const double left,
            const Expression<R> & right
        );
        
        template<typename C>
        auto ifElse(
            const Expression<C> & condition,
            const double left,
            const double right
        );
       
    } // namespace Operator
   
    #include "Expression.tpp"

}  } // namespace Expression, namespace Valencia

#endif
