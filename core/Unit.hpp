#ifndef UNIT_H
#define UNIT_H

namespace Valencia {
    
    // Represents the unit of a physical quantity
    struct Unit {
        const char * name;      // e.g. Volt
        const char * symbol;    // e.g. V
        const char * quantity;  // e.g. Voltage
        const bool allowPrefix; // e.g. print 1e-9 m as "nm" (Nanometre)
        
        Unit(
            const char * name,
            const char * symbol,
            const char * quantity,
            const bool allowPrefix = false
        ) : name(name), symbol(symbol), quantity(quantity), allowPrefix(allowPrefix) {}
        virtual ~Unit() {}
    };

    // Static global units
    static const Unit one_u     { "One",     "1",    "Quantity",            /*allowPrefix*/ true };
    static const Unit index_u   { "One",     "1",    "Index",               /*allowPrefix*/ true };
    static const Unit second_u  { "Second",  "s",    "Time",                /*allowPrefix*/ true };
    static const Unit volt_u    { "Volt",    "V",    "Voltage",             /*allowPrefix*/ true };
    static const Unit ampere_u  { "Ampere",  "A",    "Electrical Current",  /*allowPrefix*/ true };
    static const Unit metre_u   { "Metre",   "m",    "Distance",            /*allowPrefix*/ true };
    static const Unit kelvin_u  { "Kelvin",  "K",    "Temperature",         /*allowPrefix*/ true };
    static const Unit hertz_u   { "Hertz",   "Hz",   "Frequency",           /*allowPrefix*/ true };
    static const Unit ohm_u     { "Ohm",     "Ω",    "Resistance",          /*allowPrefix*/ true };
    static const Unit sievert_u { "Sievert", "S",    "Conductance",         /*allowPrefix*/ true };

    static const Unit eV_u            { "Electron Volt",   "eV",            "Energy",               true };
    static const Unit kelvinPerWatt_u { "Kelvin per Watt", "K\u2009W⁻¹",    "Thermal resistance"         };
    static const Unit perCubicMetre_u { "Per cubic metre", "m⁻³",           "Density",              true };
    
} // namespace Valencia

#endif