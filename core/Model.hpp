#ifndef MODEL_H
#define MODEL_H

#include "Expression.hpp"

#include "Parameter.hpp"
#include "Utils.hpp"
#include <new>
#include <cstring>
#include <vector>

namespace Valencia {
    
    #define RESULT(name, unit, callback) ModelResult name { maxSize, &size, #name, callback, unit, nullptr };
    #define RESULT_CB(action) \
    [](double * data, const SimulationState * state, const Model * m) -> void { \
        data[state->i] = action; \
    }
    
    // All Model implementations reside in the namespace UsingOperator to have
    // access to all Expressions and operator overloadings.
    namespace UsingOperator {
        namespace O = Valencia::Expression::Operator;
        using namespace O;
    }

    struct Model;
    typedef double rdouble_t;
    typedef void (*ModelResultReader)(double *, const SimulationState *, const Model *);

    /* ============= */
    /* === Model === */
    /* ============= */
     
    // Forward declaration of ModelOutput
    class ModelOutput;

    // Base class of all VCM models to be used with Valencia
    struct Model : public Parameterized {
        template<typename... P>
        Model(P&... parameters) : Parameterized(parameters...) {}
        virtual ~Model() = default;

        // Common physical constants
        static constexpr double e     { 1.602176634e-19 };    // Electron charge [C]
        static constexpr double m_e   { 9.1093837015e-31 };   // Electron rest mass [kg]
        static constexpr double h     { 6.62607015e-34 };     // Planck constant [J*s]
        static constexpr double pi    { 3.1415926536 };       // Pi
        static constexpr double k_B   { 1.380649e-23 };       // Boltzman constant [J/K]
        static constexpr double eps_0 { 8.8541878128e-12 };   // Vacuum permittivity [F/m]
        
        // virtual void applyVoltage(const double V_in, const double dt) = 0;
        virtual ModelOutput * createOutput(const std::size_t maxSize) const = 0;
        
        template<typename... E>
        static auto wrapEquations(const E&... expressions) {
            return Expression::Expressions<E...>(std::forward_as_tuple(expressions...));
        }

        template<typename... A>
        static auto wrapArguments(const A&... arguments) {
            return std::forward_as_tuple(arguments...);
        }
    };
    
    /* =================== */
    /* === ModelResult === */
    /* =================== */

    struct ModelResult {
        const char * name;
        const ModelResultReader reader;
        const Unit & unit;          
        const char * description;   // may be nullptr
        
        ModelResult(
            const std::size_t maxSize,
            std::size_t * size,
        
            const char * name,
            const ModelResultReader reader,
            const Unit & unit,
            const char * description
        );
        
        virtual ~ModelResult();
        
        void put(const SimulationState * state, const Model * model);
        
        // Mainly used by PlotView::Plotables
        double * getData() const { return data; }
        std::size_t * getSizePtr() const { return size; }
        std::size_t getSize() const { return *size; }
        
        protected:
            std::size_t * size;
            double * data;
    };

    /* =================== */
    /* === ModelOutput === */
    /* =================== */
    
    class ModelOutput {
        public:
            template<typename... R>
            ModelOutput(const std::size_t maxSize, R&... resultPack) : 
                maxSize(maxSize), count(4 + sizeof...(R)) {
                // Must be in constructor body
                results = new ModelResult *[count] {
                    &i, &t, &dt, &U_src, &resultPack...
                };
            }
            virtual ~ModelOutput();
            
            ModelResult * getResult(const std::size_t & index);
            ModelResult * findResult(const char * name);
            
            ModelResult ** getResults();
            std::size_t getResultCount() const;
            
            void writeResults(const char * fileName);
            void writeResults(
                const char * fileName, const std::vector<std::string> & resultNames
            );
            
            void putResults(const SimulationState * state, const Model * model);
            
            std::size_t size { 0 };    // updated by simulation
            const std::size_t maxSize; // must not be reference

        private:
            // General results shared by/independent of models
            RESULT(i,    index_u, RESULT_CB( state->i  ))
            RESULT(t,   second_u, RESULT_CB( state->t  ))
            RESULT(dt,  second_u, RESULT_CB( state->dt ))
            RESULT(U_src, volt_u, RESULT_CB( state->V  ))
            // V_in, I, ..?
        
            // Fake ModelResult struct only used for searching
            struct ResultSearch {
                const char * name;
                ResultSearch(const char * search) : name(search) {}
                virtual ~ResultSearch() = default; /* virtual destructor required! */
            };
            
            bool sort();
            static int compare (const void * ptr1, const void * ptr2);
            ModelResult * const * findResultInternal(const char * name);
            bool findSortedResultIndex(const char * name, std::size_t & index);
            
            void writeResultsInternal(
                const char * fileName, ModelResult ** results, 
                const std::size_t count
            );
            
            // Sorted results for efficient (binary) search-by-name
            ModelResult ** sortedResults {};
            ModelResult ** results {};
            const std::size_t count;
    };
    
} // namespace Valencia

#endif