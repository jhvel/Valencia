/* Included by Simulation.hpp */

template<typename M>
Simulation<M>::Simulation(M * model, const std::size_t maxOutputSize) : 
    Simulation(model, model->createOutput(maxOutputSize), true) {}
    
template<typename M>
Simulation<M>::Simulation(M * model, ModelOutput * output) :
    Simulation(model, output, false) {}
    
template<typename M>
Simulation<M>::Simulation(
    M * model, ModelOutput * output, const bool ownsOutput
) : SimulationInterface(output, ownsOutput),
    model(model),
    system(model->getEquations(), model->getArguments()) {}
    
template<typename M>
ModelOutput * Simulation<M>::run(
    const VoltageSource & voltageSource, 
    const bool resetModelState
) {
    SimulationState state;
    output->size = 0;
    
    if (resetModelState)
        model->resetState();
    
    double t { 0 };
    double dt { dt_max };
    std::size_t i { 0 };
    
    useCurrentCompliance = false;
    model->setCurrentComplianceEnabled(false);
    
    while (!voltageSource.isFinished(t)) {
        const double U_src { voltageSource.getVoltage(t) };
        const State startState { model->getState() };
        
        // TODO: Move to Model?
        const double N_disc { model->getN_disc()  };
        const double absdN_discdt { std::fabs(model->getdN_disc()) };
        
        // Constraints
        const double dt_U { voltageSource.getNextCriticalTime(t) - t };
        const double dt_N { 
            absdN_discdt > 1e-15? dN_max * N_disc / absdN_discdt : dt_max // N_disc change
        };
        const double dt_raise { dt_max_raise_factor * dt };               // dt change
        const double absI_diff { std::fabs(model->J_ion_diff(model->getStateAsVariables())) };
        // const double dt_I_diff { absI_diff > 1e-15? I_diff_scale / absI_diff : dt_max };
        
        
        // printf_s("t: %e, dt: %e, U_src: %e | abs dN_disc dt: %.10e \n", t, dt, U_src, abs_dNdt);
            
        State newState { startState };
        dt = std::min(static_cast<double>(dt_max), std::min(std::min(dt_N, dt_raise), dt_U));
        
        double lambda { 1.0 };
        
        while (true) {
            try {
                newState = update(startState, U_src, dt);
                break;
            } catch (std::runtime_error error) {
                if (lambda > 0.1) {
                    lambda *= 0.5;
                    dt *= lambda;
                } else {
                    printf_s("Simulation error occurred\n");
                    throw;
                }
            }
        }
        
        model->setState(newState);
        
        state.i = i;
        state.t = t;
        state.dt = dt;
        state.V = U_src;
        
        if (i < output->maxSize) {
            output->putResults(&state, model);
            output->size = i + 1;
        } else
            break;

        t += dt;        
        i++;
    }
    return output;
}

template<typename M>
decltype(std::declval<M>().getState()) Simulation<M>::update(const State & startState, const double U_src, const double dt) {
    if (useCurrentCompliance)
        model->setCurrent(U_src < 0? I_min : I_max, dt);
    else
        model->setVoltage(U_src, dt);
    
    State newState { system.solve(startState, y_norm_max, k_max) }; // throws std::runtime_error
    
    const double absI { std::fabs(newState[0]) };
    const double absU { std::fabs(newState[1]) };
    
    if (!useCurrentCompliance && absI > std::fabs(U_src < 0? I_min : I_max)) {
        useCurrentCompliance = true;
        model->setCurrentComplianceEnabled(true);
        // printf_s("t: %e, dt: %e, U_src: %e, absI: %e | switching compliance on\n", t, dt, U_src, absI);
        return update(startState, U_src, dt);
    } else if (useCurrentCompliance && absU >= std::fabs(U_src)) {
        useCurrentCompliance = false;
        model->setCurrentComplianceEnabled(false);
        // printf_s("t: %e, dt: %e, U_src: %e | switching compliance off\n", t, dt, U_src);
        return update(startState, U_src, dt);
    }
    
    return newState;    
}