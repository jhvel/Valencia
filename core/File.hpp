#ifndef FILE_H
#define FILE_H

#include <cstdio>

#include "StringBuffer.hpp"
// #include <tinyfiledialogs.h>

namespace Valencia {
    
    /* ============ */
    /* === File === */
    /* ============ */

    struct File {
        typedef int Mode;

        enum Mode_ {
            Mode_Read  = 0,
            Mode_Write = 1
        };
        
        File(const char * fileName, Mode mode);
        ~File();

        operator std::FILE *() const { 
            return file;
        }
        
        bool canWrite() const;
        
        private:
            Mode mode;
            const char * fileName;
            std::FILE * file {};
    };

    /* ======================== */
    /* === FileStringBuffer === */
    /* ======================== */

    struct FileBuffer : public StringBuffer {
        static constexpr std::size_t DEFAULT_FLUSH_MAX_SIZE { 512 };
        
        FileBuffer(
            File & outputFile,
            std::size_t initialSize = DEFAULT_INITIAL_SIZE,
            std::size_t maxSize = DEFAULT_FLUSH_MAX_SIZE
        );
        ~FileBuffer() override;
        
        
        private:
            bool grow() override;
            File & outputFile;
            bool flushTo(File & file);
    };
    
} // namespace Valencia

#endif