#include "VCModel.hpp"

using namespace Valencia;

/* === UsingOperator::VCModel === */

UsingOperator::VCModel::VCModel() : Model(
    name,
    _T_0,
    _alpha_line,
    _C_th,
    _R_th,
    _R_th_line,     
    _R_line0,   
    _R_ICL,
    _dE_0,
    _l_cell,
    _r,
    _a,
    _mu_n,
    _nu_0,
    _N_plug,
    _N_min,
    _N_max,
    _N_disc0,
    _enableI_diff,
    _eps_r,
    _phi,  
    _phi_B0,  
    _phi_subband,
    _l_diff,
    _m_factor,
    _z
) {
    resetState();                     
}

UsingOperator::VCModel::State UsingOperator::VCModel::getInitialState() const {
    return State {/* I */ 0.0, /*V */ 0.0, N_disc0(), /* V_simmons */ 0.0 };
}

void UsingOperator::VCModel::setVoltage(const double V, const double dt) {
    state.V_src = V;
    state.dt = dt;
}

void UsingOperator::VCModel::setVoltageRateOfChange(const double dVdt) {
    state.dVdt = dVdt;
}

void UsingOperator::VCModel::setCurrent(const double I, const double dt) {
    state.I_max = I;
    state.dt = dt;
}

UsingOperator::VCModel::State UsingOperator::VCModel::getState() const {
    return state.adapter.values;
}

void UsingOperator::VCModel::setState(const State & input) {
    state.adapter.values = input; // copy data
    
    // The values of state.P_th and state.T are used by dN_disc and therefore
    // must not be updated before state.dN_disc has been calculated.
    const double P_th    { this->P_th(state.adapter.variables) };
    const double T       { this->T(state.adapter.variables) };
    const double dN_disc { this->dN_disc(state.adapter.variables) };
    
    state.T = T;
    state.P_th = P_th;
    state.dN_disc = dN_disc;
}

void UsingOperator::VCModel::resetState() {
    state.T = 0;
    state.P_th = 0;
    state.dN_disc = 0;
    
    state.V_src = 0;
    state.I_max = 0;
    state.dt = 0;
    state.adapter.values = getInitialState();
}

ModelOutput * UsingOperator::VCModel::createOutput(const std::size_t maxSize) const {
    return new VCModelOutput { maxSize };
}
   
/* === VCModelOutput === */

VCModelOutput::VCModelOutput(const std::size_t maxSize) : ModelOutput(maxSize, 
    I,
    U,       
    absI,       
    I_ion,      
    I_ion_drift,
    I_ion_diff,
    T,       
    l_depletion,
    l_tunnel,
    N_disc,     
    dN_discdt, 
    dIdt,    
    U_simmons,  
    U_plug,     
    U_series,   
    U_cell,
    dIdt_V,
    dVdt
) {}