/* Included by NewtonsMethod.hpp */

/* === Matrix === */

template<std::size_t M, std::size_t N>
Matrix<M, N>::Matrix(Array<double, M * N> & array) : data(array) {
    reset();
}

template<std::size_t M, std::size_t N>
double & Matrix<M, N>::get(const std::size_t i, const std::size_t j) { // i: row, j: column
    return data[getDataIndex(i, j)];
}

template<std::size_t M, std::size_t N>
double & Matrix<M, N>::operator () (const std::size_t i, const std::size_t j) {
    return get(i, j);
}

template<std::size_t M, std::size_t N>
const double & Matrix<M, N>::get(const std::size_t i, const std::size_t j) const { // i: row, j: column
    return data[getDataIndex(i, j)];
}

template<std::size_t M, std::size_t N>
const double & Matrix<M, N>::operator () (const std::size_t i, const std::size_t j) const {
    return get(i, j);
}

template<std::size_t M, std::size_t N>
void Matrix<M, N>::swapRows(const std::size_t i1, const std::size_t i2) {
    swap(ROWS, i1, i2);
}

template<std::size_t M, std::size_t N>
void Matrix<M, N>::swapColumns(const std::size_t j1, const std::size_t j2) {
    swap(COLS, j1, j2);
}

template<std::size_t M, std::size_t N>
double Matrix<M, N>::calcNorm() const {
    static_assert(N == 1, "calcNorm() is currently only supported for Vectors");
    
    double sum { 0 };
    for (std::size_t i { 0 }; i < M; i++)
        sum += std::pow(get(i, 0), 2.0);
    return std::sqrt(sum);
}

template<std::size_t M, std::size_t N>
void Matrix<M, N>::print() const {
    for (std::size_t i { 0 }; i < M; i++) {
        for (std::size_t j { 0 }; j < N; j++) {
            printf_s("%e\t", get(i, j));
        }
        printf_s("\n");
    }     
}

// Syntactic sugar tailored to Newton solver

template<std::size_t M, std::size_t N>
void Matrix<M, N>::operator *= (const double factor) {
    for (std::size_t k { 0 }; k < M * N; k++) {
        getDirect(k) *= factor;
    }    
}

template<std::size_t M, std::size_t N>
Matrix<M, N> & Matrix<M, N>::operator - () {
    for (std::size_t k { 0 }; k < M * N; k++) {
        getDirect(k) *= -1.0;
    }
    return *this;
}

template<std::size_t M, std::size_t N>
void Matrix<M, N>::reset() {
    for (int k = 0; k < 2; k++) {
        for (std::size_t l { 0 }; l < size[k]; l++)
            map[k][l] = l;
    }
}

template<std::size_t M, std::size_t N>
double & Matrix<M, N>::getDirect(const std::size_t k) { // i: row, j: column
    // TODO: Check index
    return data[k];
}

template<std::size_t M, std::size_t N>
const double & Matrix<M, N>::getDirect(const std::size_t k) const { // i: row, j: column
    // / TODO: Check index
    return data[k];
}

template<std::size_t M, std::size_t N>
inline std::size_t Matrix<M, N>::getDataIndex(const std::size_t & i, const std::size_t & j) const {
    // TODO: Check indices
    return map[ROWS][i] * stride[ROWS] + map[COLS][j] * stride[COLS];
}

template<std::size_t M, std::size_t N>
void Matrix<M, N>::swap(const int l, const std::size_t k1, const std::size_t k2) {
    // TODO: Check indices
    const auto cached { map[l][k1] };
    map[l][k1] = map[l][k2];
    map[l][k2] = cached;
}

/* === LinearSystem === */ 

template<std::size_t M, std::size_t N>
void LinearSystem<M, N>::solve(
    Array<double, M * N> & matrixIn,
    Array<double, M> & vectorIn,
    Array<double, M> & result
) {
    Matrix<M, N> matrix { matrixIn };
    Matrix<M, 1> vector { vectorIn };
    
    std::size_t pivotIndex {};

    // Bring matrix in LU shape
    for (std::size_t i { 0 }; i < M; i++) {
        const bool hasNonZeroPivot { findPivot(matrix, i, pivotIndex) };
    
        if (!hasNonZeroPivot)
            throw std::runtime_error("Matrix does not have full rank");

        // Swap rows so pivot is at (i, i)
        if (pivotIndex != i) {
            matrix.swapRows(i, pivotIndex);
            vector.swapRows(i, pivotIndex);
        }
        
        // Add rows
        for (std::size_t iTo { i + 1 }; iTo < M; iTo++) {
            const double factor { -matrix(iTo, i) / matrix(i, i) };
            for (std::size_t j { 0 }; j < N; j++)
                matrix(iTo, j) += matrix(i, j) * factor;
            vector(iTo, 0) += vector(i, 0) * factor;
        }
    }

    // Substitute back
    for (std::size_t i { M - 1 }; i < M; i--) { // Using wrap-around of unsigned integer type std::size_t.
        for (std::size_t k { i + 1 }; k < M; k++)
            vector(i, 0) += -matrix(i, k) * result[k];
        result[i] = vector(i, 0) / matrix(i, i);
    }
}

// old
template<std::size_t M, std::size_t N>
Vector<M> LinearSystem<M, N>::solve(
    Matrix<M, N> & matrix, Vector<M> & vector
) {
    Vector<M> result;
    std::size_t pivotIndex {};

    // Bring matrix in LU shape
    for (std::size_t i { 0 }; i < M; i++) {
        const bool hasNonZeroPivot { findPivot(matrix, i, pivotIndex) };
    
        if (!hasNonZeroPivot)
            throw std::runtime_error("Matrix does not have full rank");

        // Swap rows so pivot is at (i, i)
        if (pivotIndex != i) {
            matrix.swapRows(i, pivotIndex);
            vector.swapRows(i, pivotIndex);
        }
        
        // Add rows
        for (std::size_t iTo { i + 1 }; iTo < M; iTo++) {
            const double factor { -matrix(iTo, i) / matrix(i, i) };
            for (std::size_t j { 0 }; j < N; j++)
                matrix(iTo, j) += matrix(i, j) * factor;
            vector(iTo, 0) += vector(i, 0) * factor;
        }
    }

    // Substitute back
    for (std::size_t i { M - 1 }; i < M; i--) { // Using wrap-around of unsigned integer type std::size_t.
        for (std::size_t k { i + 1 }; k < M; k++)
            vector(i, 0) += -matrix(i, k) * result(k, 0);
        result(i, 0) = vector(i, 0) / matrix(i, i);
    }
    return result;
}

template<std::size_t M, std::size_t N>
bool LinearSystem<M, N>::findPivot(
    const Matrix<M, N> & matrix, const std::size_t iFrom,
    std::size_t & pivotIndex
) {
    double maxValue { 0 };
    for (std::size_t i { iFrom }; i < M; i++) {
        const double value { std::fabs(matrix(i, iFrom)) };
        if (value > maxValue) {
            maxValue = value;
            pivotIndex = i;
        }
    }
    return maxValue > 0;
}

/* === NonLinearSystem === */

template<typename... E, typename... A>
NonLinearSystem<
    Expression::Expressions<E...>, Expression::Arguments<A...>
>::NonLinearSystem(
    const Expression::Expressions<E...> & equations,
    const Expression::Arguments<A...> & arguments
) : f(equations), x(arguments), jacobian(f.derive(arguments)) {}

    
template<typename... E, typename... A>
template<typename C>
auto NonLinearSystem<
    Expression::Expressions<E...>, Expression::Arguments<A...>
>::solve(const Vector & x0, const C callback) {
    x.values = x0; // Copy data
    
    for (std::size_t k { 0 }; true; k++) {
        f.evaluate(x.variables, /* -> */ y);

        if (!callback(k, x.values, y, x0))
            return x.values; // gets copied
        
        jacobian.evaluate(x.variables, /* -> */ J);
        LinearSystem<N, N>::solve(J, -y, /* -> */ dx); // Destructive, J and y get modified
        
        x.values += dx;
    }
}

template<typename... E, typename... A>
auto NonLinearSystem<
    Expression::Expressions<E...>, Expression::Arguments<A...>
>::solve(
    const Vector & x0, const double maxNorm, const double maxIterationCount
) {
    x.values = x0; // Copy data
    
    for (std::size_t k { 0 }; true; k++) {
        f.evaluate(x.variables, /* -> */ y);
        
        if (k > maxIterationCount)
            throw std::runtime_error("Maximum iteration count exceeded");
        
        const double norm { calcNorm(y) };
        
        if (std::isnan(norm))
            throw std::runtime_error("Norm is nan");

        if (norm <= maxNorm)
            return x.values; // gets copied
        
        jacobian.evaluate(x.variables, /* -> */ J);
        LinearSystem<N, N>::solve(J, -y, /* -> */ dx); // Destructive, J and y get modified
        
        x.values += dx;
    }
}
