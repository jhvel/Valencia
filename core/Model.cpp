#include "Model.hpp"
#include "File.hpp"

using namespace Valencia;

/* =================== */
/* === ModelResult === */
/* =================== */

ModelResult::ModelResult(
    const std::size_t maxSize,
    std::size_t * size,

    const char * name,
    const ModelResultReader reader,
    const Unit & unit,
    const char * description
) : size(size), name(name), reader(reader), unit(unit), 
    description(description) {
        
    data = new double[maxSize];
};
 
ModelResult::~ModelResult() {
    delete [] data;
}

// TODO: Directly access reader?
void ModelResult::put(const SimulationState * state, const Model * model) {
    reader(data, state, model);
}

/* =================== */
/* === ModelOutput === */
/* =================== */

ModelOutput::~ModelOutput() {
    delete [] sortedResults;
    delete [] results;
    // delete [] data;
}

ModelResult ** ModelOutput::getResults() {
    return results;
}

std::size_t ModelOutput::getResultCount() const {
    return count;
}

ModelResult * const * ModelOutput::findResultInternal(const char * name) {
    if (!sortedResults && !sort())
        throw std::runtime_error("Could not sort results, unable to search");
    
    const ResultSearch search(name);
    const auto searchPtr { reinterpret_cast<const ModelResult *>(&search) };
    const auto resultPtr { static_cast<ModelResult * const *>(
        std::bsearch(&searchPtr, sortedResults, count, 
            sizeof(ModelResult *), compare)
    )};
    return resultPtr;
}

ModelResult * ModelOutput::findResult(const char * name) {
    const auto resultPtr { findResultInternal(name) };
    return resultPtr? *resultPtr : nullptr;
}

bool ModelOutput::findSortedResultIndex(const char * name, std::size_t & index) {
    const auto resultPtr { findResultInternal(name) };
    if (resultPtr)
        index = resultPtr - sortedResults;
    return resultPtr;
}

ModelResult * ModelOutput::getResult(const std::size_t & index) {
     return index < getResultCount()?
        results[index] : nullptr;
}

void ModelOutput::writeResults(const char * fileName) {
    writeResultsInternal(fileName, results, count);
}

void ModelOutput::writeResults(
    const char * fileName, const std::vector<std::string> & resultNames
) {
    const std::size_t count { resultNames.size() };
    ModelResult ** results { new ModelResult *[count] };
    
    for (std::size_t i { 0 }; i < count; i++) {
        results[i] = findResult(resultNames[i].c_str());
        if (!results[i]) {
            printf_s("Did not find result named %s\n", resultNames[i].c_str());
            throw std::runtime_error("Writing resultss failed");
        }
    }
    
    writeResultsInternal(fileName, results, count);
    delete [] results;
}

void ModelOutput::writeResultsInternal(
    const char * fileName, ModelResult ** results, const std::size_t count
) {
    File file(fileName, File::Mode_Write);
    FileBuffer bufferedFile(file);

    // Write header
    for (std::size_t i { 0 }; i < count; i++) {
        bufferedFile.put(results[i]->name).put(" [%s]", results[i]->unit.symbol);
        bufferedFile.put('\t');
    }

    bufferedFile.trim('\t');
    bufferedFile.put('\n');

    // Write data
    for (std::size_t k { 0 }; k < size; k++) {
        for (std::size_t i { 0 }; i < count; i++) {
            bufferedFile.put(results[i]->getData()[k]).put('\t');
        }
        bufferedFile.trim('\t');
        bufferedFile.put('\n');
    }
}

void ModelOutput::putResults(const SimulationState * state, const Model * model) { 
    for (std::size_t i { 0 }; i < count; i++)
        getResult(i)->put(state, model);
}

bool ModelOutput::sort() {
    if (sortedResults)
        return true;
    
    sortedResults = new (std::nothrow) ModelResult *[count];
    if (sortedResults) {
        for (int i = 0; i < count; i++) 
            sortedResults[i] = results[i];
        
        std::qsort(sortedResults, count, sizeof(ModelResult *), compare);
        return true;
    }
    return false;
}

int ModelOutput::compare(const void * ptr1, const void * ptr2) {
    const auto result1 { *static_cast<ModelResult * const *>(ptr1) };
    const auto result2 { *static_cast<ModelResult * const *>(ptr2) };
    
    const int diff { std::strcmp(result1->name, result2->name) };
    // printf_s("%s vs. %s = %d\n", result1->name, result2->name, diff);
    return diff;
}
