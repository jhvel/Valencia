#include "VoltageSource.hpp"

using namespace Valencia;

/* ==================== */
/* === VoltageSweep === */
/* ==================== */

VoltageSweep::VoltageSweep(float U_max_f, float t_max_f) 
    : VoltageSource(name, U_max, t_max)
{
   U_max.setDefaultValue(U_max_f);
   t_max.setDefaultValue(t_max_f);
}

double VoltageSweep::getVoltage(double t) const {
    if (t <= t_max * 0.25)
        return -U_max * (4.0 * t / t_max);
    else if (t <= t_max * 0.75)
        return U_max * (4.0 * t / t_max - 2.0);
    else if (t <= t_max)
        return -U_max * (4.0 * t / t_max - 4.0);
    else
        return 0;
}

/*
double VoltageSweep::getVoltageRateOfChange(double t) const {
    if (t <= t_max * 0.25)
        return -U_max * (4.0 / t_max);
    else if (t <= t_max * 0.75)
        return U_max * (4.0 / t_max);
    else if (t <= t_max)
        return -U_max * (4.0 / t_max);
    else
        return 0;
}
*/

double VoltageSweep::getNextCriticalTime(double t) const {
    if (t < t_max * 0.25)
        return t_max * 0.25;
    else if (t < t_max * 0.5)
        return t_max * 0.5;
    else if (t < t_max * 0.75)
        return t_max * 0.75;
    else
        return t_max;
}

bool VoltageSweep::isFinished(const double t) const {
    return t >= t_max;
}

double VoltageSweep::getMaxVoltage() const {
    return U_max;
}

double VoltageSweep::getDuration() const {
    return t_max;
}


/* ==================== */
/* === VoltagePulse === */
/* ==================== */

VoltagePulse::VoltagePulse(float U_max_f, float /*ignored*/) 
    : VoltageSource(name, U_max, dt_raise, dt_hold, dt_lower, dt_idle, dt_slope)
{
    U_max.setDefaultValue(U_max_f);
    // t_max.setDefaultValue(t_max_f);
}

double VoltagePulse::getVoltage(double t) const {
    if (t < dt_raise) // raise voltage linearly
        return (t / dt_raise) * U_max;
    else if (t <= dt_raise + dt_hold) // hold voltage
        return U_max;
    else if (t < dt_raise + dt_hold + dt_lower) // lower voltage
        return (1 - (t - dt_raise - dt_hold) / dt_lower) * U_max;
    else
        return 0; // until t_max: U = 0V
}

/* 
double VoltagePulse::getVoltageRateOfChange(double t) const {
    if (t <= dt_raise) // raise voltage linearly
        return U_max / dt_raise;
    else if (t <= dt_raise + dt_hold) // hold voltage
        return 0;
    else if (t <= dt_raise + dt_hold + dt_lower) // lower voltage
        return -U_max / dt_lower;
    else
        return 0; // until t_max: U = 0V
}
*/

double VoltagePulse::getNextCriticalTime(double t) const {
    if (t < dt_slope)
        return dt_slope;
    if (t < dt_raise)
        return dt_raise;
    else if (t < dt_raise + dt_hold)
        return dt_raise + dt_hold;
     else if (t < dt_raise + dt_hold + dt_slope)
        return dt_raise + dt_hold + dt_slope;
    else if (t < dt_raise + dt_hold + dt_lower)
        return dt_raise + dt_hold + dt_lower;
    else
        return getDuration();
}

bool VoltagePulse::isFinished(const double t) const {
    return t > getDuration();
}

double VoltagePulse::getMaxVoltage() const {
    return U_max;
}

double VoltagePulse::getDuration() const {
    return dt_raise + dt_hold + dt_lower + dt_idle;
}

/* =================== */
/* === VoltageStep === */
/* =================== */

VoltageStep::VoltageStep(float U_max_f, float t_max_f) 
    : VoltageSource(name, U_max, dt_raise, t_max, dt_slope)
{
    U_max.setDefaultValue(U_max_f);
    t_max.setDefaultValue(t_max_f);
}

double VoltageStep::getVoltage(double t) const {
    if (t < dt_raise) // raise voltage linearly
        return (t / dt_raise) * U_max;
    else if (t <= t_max) // hold voltage
        return U_max;
    else
        return 0; // until t_max: U = 0V
}

/* 
double VoltageStep::getVoltageRateOfChange(double t) const {
    if (t <= dt_raise) // raise voltage linearly
        return U_max / dt_raise;
    else if (t <= dt_raise + dt_hold) // hold voltage
        return 0;
    else if (t <= dt_raise + dt_hold + dt_lower) // lower voltage
        return -U_max / dt_lower;
    else
        return 0; // until t_max: U = 0V
}
*/

double VoltageStep::getNextCriticalTime(double t) const {
    if (t < dt_slope)
        return dt_slope;
    if (t < dt_raise)
        return dt_raise;
    else
        return getDuration();
}

bool VoltageStep::isFinished(const double t) const {
    return t > getDuration();
}

double VoltageStep::getMaxVoltage() const {
    return U_max;
}

double VoltageStep::getDuration() const {
    return t_max;
}
