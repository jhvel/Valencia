#ifndef UTIL_H
#define UTIL_H

namespace Valencia {
    
    namespace Tuple {
        // For tuple<T...> & (reference)
        template<std::size_t I = 0, typename F, typename... T>
        inline typename std::enable_if<I == sizeof...(T), void>::type 
        forEach(std::tuple<T...> &, const F &) {}

        template<std::size_t I = 0, typename F, typename... T>
        inline typename std::enable_if<I < sizeof...(T), void>::type 
        forEach(std::tuple<T...> & tuple, const F & function) {
            function(I, std::get<I>(tuple));
            forEach<I + 1, F, T...>(tuple, function);
        }

        // For const tuple<T...> & (const reference)
        template<std::size_t I = 0, typename F, typename... T>
        inline typename std::enable_if<I == sizeof...(T), void>::type 
        forEach(const std::tuple<T...> &, const F &) {}

        template<std::size_t I = 0, typename F, typename... T>
        inline typename std::enable_if<I < sizeof...(T), void>::type 
        forEach(const std::tuple<T...> & tuple, const F & function) {
            function(I, std::get<I>(tuple));
            forEach<I + 1, F, T...>(tuple, function);
        }
    }

    template<typename T, std::size_t S>
    struct Array { 
        template<typename... I>
        Array(const I&... items) : items { items... } {}
        
        T & operator [] (const std::size_t i) {
            return items[i];
        }
        
        const T & operator [] (const std::size_t i) const {
            return items[i];
        }
        
        void operator = (const Array<T, S> & source) {
            for (std::size_t i { 0 }; i < S; i++)
                items[i] = source[i];
        }
        
        void operator += (const Array<T, S> & source) {
            for (std::size_t i { 0 }; i < S; i++)
                items[i] += source[i];
        }
        
        void operator -= (const Array<T, S> & source) {
            for (std::size_t i { 0 }; i < S; i++)
                items[i] -= source[i];
        }
        
        void operator *= (T value) {
            for (std::size_t i { 0 }; i < S; i++)
                items[i] *= value;
        }
        
        Array<T, S> & operator - () {
            for (std::size_t i { 0 }; i < S; i++)
                items[i] = -items[i];
            return *this;
        }
        
        T items[S] {};
    };

    template<std::size_t N>
    double calcNorm(const Array<double, N> & array) {
        double sum { 0 };
        for (std::size_t i { 0 }; i < N; i++)
            sum += std::pow(array[i], 2.0);
        return std::sqrt(sum);
    }
    
} // namespace Valencia



#endif