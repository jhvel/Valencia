#ifndef STRING_BUFFER_H
#define STRING_BUFFER_H

#include <cstdio>
#include <string>
#include <stdexcept>

namespace Valencia {
    
    /* ==================== */
    /* === StringBuffer === */
    /* ==================== */

    struct StringBuffer {
        static constexpr std::size_t DEFAULT_INITIAL_SIZE { 16 };
        static constexpr std::size_t DEFAULT_MAX_SIZE { 1024 };
        
        StringBuffer();
        StringBuffer(std::size_t initialSize, std::size_t maxSize);
        virtual ~StringBuffer();
        
        template<typename T>
        StringBuffer & put(T data) {
            // Gets overridden by specialized templates        
            raise("StringBuffer: put() not implemented for type");
        }
        template<typename... A>
        StringBuffer & put(const char * format, A... args) {
            return putStringFormatted(format, args...);
        }
        StringBuffer & put(
            const StringBuffer & other,
            const std::size_t & start,
            const std::size_t & end
        );
        
        template<typename... A>
        StringBuffer & putStringFormatted(const char * format, A... args);
        StringBuffer & putChar(const char c);
        StringBuffer & putString(const char * string);
        StringBuffer & putString(const char * string, const std::size_t & size);
        StringBuffer & putInt(const int value);
        StringBuffer & putDouble(
            const double & value,
            const bool trimDecimals = true
            // ...
        );
        StringBuffer & putCharRepeated(const char c, int n);
        bool putFileLine(std::FILE * file);
        void putTerminator();
        
        StringBuffer & putBuffer(const StringBuffer & other);
        StringBuffer & putBuffer(
            const StringBuffer & other,
            const std::size_t & start,
            const std::size_t & end
        );
        
        // Trimming generally only works for the last put() function call.
        // If there is an output file, previous buffer contents may have already 
        // been flushed to disc. Trimming files is not supported.
        StringBuffer & trim(const char c);
        StringBuffer & trimWhiteSpace();
        
        bool find(const char c, std::size_t & index, const std::size_t offset = 0) const;
        StringBuffer & replaceAll(const char c, const char by);
        
        const char getChar(std::size_t index) const;
        const char * getString() const;
        std::size_t getStringSize() const;
        
        bool endsWith(const char c) const;
        bool endsWithWhiteSpace() const;
        
        bool startsWith(const char * string) const;
        
        std::size_t getRemainingSize() const;
        std::size_t getTotalSize() const;
        bool isEmpty() const;
        
        void reset();
            
        protected:
            bool hasError { false };
            
            virtual bool grow();
            
            template<typename... A>
            [[ noreturn ]] void raise(const char * format, A... args) {
                // TODO: Format string for more useful error messages
                hasError = true;   
                throw std::runtime_error(format);
            }
                
        private:
            std::size_t size {};
            const std::size_t maxSize {};
            std::size_t seek { 0 };
            char * buffer {};
            
            void init();
            char * get() const;
            void seekBy(int by);
            void seekToEnd();
            
            std::size_t fitRangeSize(
                const std::size_t start,
                const std::size_t end
            );
    };

    /* Template functions */

    template<typename... A>
    StringBuffer & StringBuffer::putStringFormatted(const char * format, A... args) {
        if (format) {
            const std::size_t size { getRemainingSize() };
            const int advance { std::snprintf(get(), size, format, args...) };
            
            if (advance >= size) {
                putTerminator(); /* Override truncated data */
                if (grow())
                    return putStringFormatted(format, args...);
            } else if (advance >= 0) {
                seekBy(advance);
                return *this;
            }
        }
        raise("Error in putStringFormatted()");
    }

    /* Specialized templates for put() function */

    template<> inline StringBuffer & StringBuffer::put<int>(int value) {
        return putInt(value);
    }
    template<> inline StringBuffer & StringBuffer::put<char>(char value) {
        return putChar(value);
    }
    template<> inline StringBuffer & StringBuffer::put<double>(double value) {
        return putDouble(value);
    }
    template<> inline StringBuffer & StringBuffer::put<const char *>(const char * value) {
        return putString(value);
    }
    template<> inline StringBuffer & StringBuffer::put<std::string>(std::string value) {
        return putString(value.c_str());
    }
    template<> inline StringBuffer & StringBuffer::put<const StringBuffer &>(const StringBuffer & other) {
        return putBuffer(other);
    }

    /* ================= */
    /* === Tokenizer === */
    /* ================= */

    struct Tokenizer {
        template<typename C>
        static void tokenize(
            const StringBuffer & buffer,
            const char separator,
            C callback,
            bool trim
        ) {
            const std::size_t stringSize { buffer.getStringSize() };
            std::size_t start { 0 };
            std::size_t end { 0 };
            bool readOn { true };
             
            while (readOn && buffer.find(separator, end, start)) { 
                const bool skip { trim && !seekStart(buffer, start, end) };
                readOn = skip || callback(buffer, start, end);
                start = end + 1;
            }
             
            if (readOn && end != stringSize) {
                const bool skip { trim && !seekStart(buffer, start, stringSize) };  
                readOn = skip || callback(buffer, start, stringSize);
            }
        }
        
        private:
            static bool seekStart(const StringBuffer & buffer, std::size_t & start, 
                                  const std::size_t & end);
        
    };
    
} // namespace Valencia

#endif