#ifndef SIMULATION_H
#define SIMULATION_H

#include "VoltageSource.hpp"
#include "NewtonsMethod.hpp"

namespace Valencia {
    
    struct SimulationInterface : public Parameterized {
        SimulationInterface(ModelOutput * output = nullptr, const bool ownsOutput = false) : Parameterized(
            name,
            dt_max,
            y_norm_max,
            k_max,
            dN_max,
            // I_diff_scale,
            dt_max_raise_factor,
            I_min,
            I_max
        ),
        output(output),
        ownsOutput(ownsOutput) {}
        
        virtual ~SimulationInterface() {
             if (ownsOutput)
                delete output;
        };
        
        virtual ModelOutput * run(
            const VoltageSource & voltageSource,
            const bool resetModelState = true
        ) { return output; };
        
        // Parameters
        PARAMETER(std::string, name,       "",       nullptr,          "Name of this parameter set")
        PARAMETER(double, dt_max,          1e-3,     &second_u,        "Maximum time step"         )
        PARAMETER(double, y_norm_max,     1e-12,     &one_u,           "Maximum allowed norm of f(x)" )
        PARAMETER(double, k_max,          5,         &one_u,           "Maximum iteration count"   )
        PARAMETER(double, dN_max,         3e-3,      &one_u,           "Factor for maximum allowed change of N_disc per time step" ) 
        // PARAMETER(double, I_diff_scale,         1,   &one_u,             "Diffusion current scale factor" )
        PARAMETER(double, dt_max_raise_factor, 1.02, &one_u,           "Maximum relative change of dt per step" )
        PARAMETER(double, I_min,               -1,   &ampere_u,        "Maximum negative current (SET)" )
        PARAMETER(double, I_max,                1,   &ampere_u,        "Maximum positive current (RESET)" )
        
        
        ModelOutput * output {};
        const bool ownsOutput;
    };
    
    template<typename M>
    struct Simulation : public SimulationInterface {
        using State = decltype(std::declval<M>().getState());
        
        Simulation(M * model, const std::size_t maxOutputSize);
        Simulation(M * model, ModelOutput * output);    
        
        ModelOutput * run(
            const VoltageSource & voltageSource,
            const bool resetModelState = true
        ) override;
        
        State update(
            const State & startState, const double U_src, const double dt
        );

        private:
            using SystemOfEquations = NonLinearSystem<
                decltype(std::declval<M>().getEquations()), 
                decltype(std::declval<M>().getArguments())
            >;
            
            Simulation(M * model, ModelOutput * output, const bool ownsOutput);
        
            M * model {};
            SystemOfEquations system;
            
            bool useCurrentCompliance { false };
    };
    
    #include "Simulation.tpp"
    
} // namespace Valencia

#endif