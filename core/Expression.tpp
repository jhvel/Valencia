/* Included by Expression.hpp */

/* === Variable === */
    
template<std::size_t I>
Variable<I>::Variable(const double value) : 
    value(value) {}

template<std::size_t I>
double Variable<I>::getValue() const {
    return value;
}

/* === ReferenceVariable === */

template<std::size_t I>
ReferenceVariable<I>::ReferenceVariable(double * value) : 
    value(value) {}

template<std::size_t I>
double ReferenceVariable<I>::getValue() const {
    return *value;
}

/* === VariableAdapter === */

template<typename... A>
template<std::size_t I>
ReferenceVariable<I> VariableAdapter<A...>::convertArgument(
    const int index, const ArgumentExpression<I> &
) {
    return ReferenceVariable<I> { &values[index] };
}

template<typename... A>
template<std::size_t... N>
auto VariableAdapter<A...>::convertArguments(
    const std::tuple<A...> & arguments, const std::index_sequence<N...>
) {
    return std::make_tuple(convertArgument(N, std::get<N>(arguments))...);
}

template<typename... A>
template<std::size_t... N>
auto VariableAdapter<A...>::convertArguments(const std::tuple<A...> & arguments) {
    return convertArguments(arguments, std::index_sequence_for<A...>{});
}

template<typename... A>
VariableAdapter<A...>::VariableAdapter(const std::tuple<A...> & arguments) : 
    variables(convertArguments(arguments)) {}

/* === Expression === */

template<typename T>
Expression<T>::Expression(const T & expression) : 
    expression(expression) {}
    
template<typename T>
Expression<T>::Expression() : 
    expression() {}

template<typename T>
template<typename... V>
auto Expression<T>::evaluate(const Variables<V...> & variables) const {
    return expression.evaluate(variables);
}
    
template<typename T>
template<std::size_t I>
auto Expression<T>::derive(const Argument<I> & argument) const {
    return expression.derive(argument);
}

template<typename T>
template<typename... V>
double Expression<T>::operator () (const V&... variables) const {
    return expression.evaluate(std::forward_as_tuple(variables...));
}

template<typename T>
template<typename... V>
double Expression<T>::operator () (const Variables<V...> & variables) const {
    return expression.evaluate(variables);
}

template<typename T>
template<std::size_t I>
auto Expression<T>::derive(const ArgumentExpression<I> & other) const {
    return expression.derive(other.getArgument());
}

template<typename T>
auto Expression<T>::operator - () const {
    return *this * -1;
}

template<typename T>
void Expression<T>::print() const {
    expression.print();
}

/* === Argument === */ 

template<std::size_t I>
void Argument<I>::print() const {
    printf_s("arg<%d>", I);
}

template<std::size_t I>
template<typename... V>
double Argument<I>::evaluate(const Variables<V...> & variables) const {
    constexpr int index { findVariableById<0, I, V...>() };
    static_assert(index != -1, "Argument not contained in variable list");
    return std::get<index>(variables).getValue();
}

template<std::size_t I>
template<std::size_t J>
auto Argument<I>::derive(const Argument<J> & /* withRespectTo */) const {
    return Expression<typename std::conditional<I == J, One, Zero>::type>();
}

template<std::size_t I>
Variable<I> Argument<I>::assignValue(const double value) const {
    return Variable<I>(value);
}

/* === NonArgument === */

template<std::size_t I>
Expression<Zero> NonArgument::derive(const Argument<I> &) const {
   return Expression<Zero>();
}

/* === Constant === */

template<typename... V>
double Constant::evaluate(const Variables<V...> &) const {
    return value;
}

/* === Parameter === */

template<typename... V>
double Parameter::evaluate(const Variables<V...> &) const {
    return *value;
}

/* === Error === */

template<typename... V>
double Error::evaluate(const Variables<V...> &) const {
    throw std::runtime_error(message);
}

/* === ArgumentExpression === */

template<std::size_t I>
Variable<I> ArgumentExpression<I>::operator = (const double value) const {
    return getArgument().assignValue(value);
}

template<std::size_t I>
const Argument<I> & ArgumentExpression<I>::getArgument() const {
    return this->expression;
}

/* === UnaryExpression === */

template<typename I, typename O>
UnaryExpression<I, O>::UnaryExpression(const Expression<I> & inner) :
    inner(inner) {}

template<typename I, typename O>
template<typename... V>
double UnaryExpression<I, O>::evaluate(const Variables<V...> & variables) const {
    return O::evaluate(inner, variables);
}

template<typename I, typename O>
template<std::size_t N>
auto UnaryExpression<I, O>::derive(const Argument<N> & argument) const {
    return O::derive(inner, argument);
}

template<typename I, typename O>
void UnaryExpression<I, O>::print() const {
    return O::print(inner);
}

/* === BinaryExpression === */

template<typename L, typename R, typename O>
BinaryExpression<L, R, O>::BinaryExpression(
    const Expression<L> & left, 
    const Expression<R> & right
) : left(left), right(right) {}

template<typename L, typename R, typename O>
template<typename... V>
double BinaryExpression<L, R, O>::evaluate(const Variables<V...> & variables) const {
    return O::evaluate(left, right, variables);
}

template<typename L, typename R, typename O>
template<std::size_t I>
auto BinaryExpression<L, R, O>::derive(const Argument<I> & argument) const {
    return O::derive(left, right, argument);
}

template<typename L, typename R, typename O>
void BinaryExpression<L, R, O>::print() const {
    return O::print(left, right);
}

/* == ConditionalExpression == */
    
template<typename C, typename L, typename R>
ConditionalExpression<C, L, R>::ConditionalExpression(
    const Expression<C> & condition,
    const Expression<L> & left,
    const Expression<R> & right
) : condition(condition), left(left), right(right) {}

template<typename C, typename L, typename R>
template<typename... V>
double ConditionalExpression<C, L, R>::evaluate(
    const Variables<V...> & v
) const {
    return condition(v)? left(v) : right(v);
}

template<typename C, typename L, typename R>
template<std::size_t I>
auto ConditionalExpression<C, L, R>::derive(const Argument<I> & a) const {
    // This is a naive implementation that may cause issues in the region 
    // where condition switches. However, unlike for min/max operators,
    // we have no information about where this region is. Use carefully.
  
    using T = ConditionalExpression<
        C, decltype(left.derive(a)), decltype(right.derive(a))>; 
    return Expression<T>(T(condition, left.derive(a), right.derive(a)));             
}


template<typename C, typename L, typename R>
void ConditionalExpression<C, L, R>::print() const {
    printf_s("if (");
    condition.print();
    printf_s(") { ");
    left.print();
    printf_s(" } else { ");
    right.print();
    printf_s(" }");
}

/* === Expressions === */

template<typename... E>
Expressions<E...>::Expressions(const std::tuple<E...> & expressions) : 
    expressions(expressions) {}

template<typename... E>
template<typename... V>
void Expressions<E...>::evaluate(
    const Variables<V...> & variables, Array<double, sizeof...(E)> & result
) const {
    Tuple::forEach(expressions, 
        [&variables, &result] (const std::size_t k, auto & expression) {
            result[k] = expression(variables);
        }
    );
}

template<typename... E>
template<typename... A>
auto Expressions<E...>::derive(const Arguments<A...> & arguments) const {
    return wrap(deriveAllForEach(std::index_sequence_for<E...>{}, arguments));
}

template<typename... E>
template<typename... V>
auto Expressions<E...>::operator () (const Variables<V...> & variables) const {
    return evaluate(variables);
}

template<typename... E>
template<typename... V>
auto Expressions<E...>::operator () (
    const Variables<V...> & variables,
    Array<double, sizeof...(E)> & result
) const {
    return evaluate(variables, result);
}

template<typename... E>
template<typename... F>
auto Expressions<E...>::wrap(const std::tuple<F...> & expressions) const {
    return Expressions<F...>(expressions);
}

template<typename... E>
template<typename... A, std::size_t... I>
auto Expressions<E...>::deriveAllForEach(
    const std::index_sequence<I...>, const Arguments<A...> & arguments
) const {
    return std::tuple_cat(
        deriveForEach(std::get<I>(expressions), arguments, 
                      std::index_sequence_for<A...>{})... 
    );
}

template<typename... E>
template<typename F, typename... A, std::size_t... I>
auto Expressions<E...>::deriveForEach(
    const F & expression,
    const Arguments<A...> & arguments,
    const std::index_sequence<I...>
) const {
    return std::make_tuple(expression.derive(std::get<I>(arguments))...);
}

namespace Operator {
    
    #define EVALUATE_UNARY_OPERATOR(NAME, EVALUATION)  \
    template<typename I, typename... V>                \
    double NAME ## _operator::evaluate(                \
        const Expression<I> & innerExpression,         \
        const Variables<V...> & v                      \
    ) {                                                \
        if (DEBUG) printf_s( #NAME " evaluate()\n");   \
        const double inner { innerExpression(v) };     \
        return EVALUATION;                             \
    }
    
    #define EVALUATE_BINARY_OPERATOR(NAME, EVALUATION) \
    template<typename L, typename R, typename... V>    \
    double NAME ## _operator::evaluate(                \
        const Expression<L> & leftExpression,          \
        const Expression<R> & rightExpression,         \
        const Variables<V...> & v                      \
    ) {                                                \
        if (DEBUG) printf_s( #NAME " evaluate()\n");   \
        const double left { leftExpression(v) };       \
        const double right { rightExpression(v) };     \
        return EVALUATION;                             \
    }
    
    #define DERIVE_UNARY_OPERATOR(NAME, DERIVATIVE) \
    template<typename I, std::size_t N>             \
    auto NAME ## _operator::derive(                 \
        const Expression<I> & inner,                \
        const Argument<N> & a                       \
    ) {                                             \
        if (DEBUG) printf_s( #NAME " derive()\n");  \
        return DERIVATIVE;                          \
    }
    
    #define DERIVE_BINARY_OPERATOR(NAME, DERIVATIVE) \
    template<typename L, typename R, std::size_t I>  \
    auto NAME ## _operator::derive(                  \
        const Expression<L> & left,                  \
        const Expression<R> & right,                 \
        const Argument<I> & a                        \
    ) {                                              \
        if (DEBUG) printf_s( #NAME " derive()\n");   \
        return DERIVATIVE;                           \
    }
    
    #define UNARY_OPERATOR(NAME, EVALUATION, DERIVATIVE) \
    EVALUATE_UNARY_OPERATOR(NAME, EVALUATION)            \
    DERIVE_UNARY_OPERATOR(NAME, DERIVATIVE)
    
    #define BINARY_OPERATOR(NAME, EVALUATION, DERIVATIVE) \
    EVALUATE_BINARY_OPERATOR(NAME, EVALUATION)            \
    DERIVE_BINARY_OPERATOR(NAME, DERIVATIVE)
    
    
    // Arithmetic operators
    BINARY_OPERATOR(add,      left + right, left.derive(a) + right.derive(a))
    BINARY_OPERATOR(subtract, left - right, left.derive(a) - right.derive(a))
    BINARY_OPERATOR(multiply,
        left && right? left * right : 0,
        left.derive(a) * right + right.derive(a) * left)
    BINARY_OPERATOR(divide, 
        left? left / right : 0, 
        (left.derive(a) * right - right.derive(a) * left) / (right * right))
        
    // Comparsion operators
    EVALUATE_BINARY_OPERATOR(greater,      left >  right)
    EVALUATE_BINARY_OPERATOR(greaterEqual, left >= right)
    EVALUATE_BINARY_OPERATOR(less,         left <  right)
    EVALUATE_BINARY_OPERATOR(lessEqual,    left <= right)
    EVALUATE_BINARY_OPERATOR(equal,        left == right)
    EVALUATE_BINARY_OPERATOR(notEqual,     left != right)
    
    // Unary functions
    UNARY_OPERATOR(exp,  std::exp(inner),  inner.derive(a) * Operator::exp(inner))
    UNARY_OPERATOR(ln,   std::log(inner),  inner.derive(a) / inner)
    UNARY_OPERATOR(sqrt, std::sqrt(inner), inner.derive(a) / (2 * Operator::sqrt(inner)))
    
    // DERIVE_UNARY_OPERATOR(abs,)

    // Trigonometric functions
    UNARY_OPERATOR(sin, std::sin(inner), inner.derive(a) *  Operator::cos(inner))
    UNARY_OPERATOR(cos, std::cos(inner), inner.derive(a) * -Operator::sin(inner))
    UNARY_OPERATOR(tan, std::tan(inner), inner.derive(a) / (Operator::cos(inner) * Operator::cos(inner)))

    // Inverse trigonometric functions
    UNARY_OPERATOR(asin, std::asin(inner), inner.derive(a) / Operator::sqrt(1 - inner * inner))
    // ...

    // Hyberpolic functions
    UNARY_OPERATOR(sinh, std::sinh(inner), inner.derive(a) *  Operator::cosh(inner))
    UNARY_OPERATOR(cosh, std::cosh(inner), inner.derive(a) *  Operator::sinh(inner))
    UNARY_OPERATOR(tanh, std::tanh(inner), inner.derive(a) / (Operator::cosh(inner) * Operator::cosh(inner)))

    BINARY_OPERATOR(pow,
        left? right? std::pow(left, right) : 1 : 0,
        Operator::pow(left, right - 1) * (left.derive(a) * right + 
        right.derive(a) * left * Operator::ln(left)))

    // Anonymous namespace to hide deriveMin() and deriveMax()
    // Both functions do return leftDerivative if left == right,
    // which is not mathematically correct. Use carefully.
    namespace {
        template<typename L, typename R, std::size_t I>
        inline auto deriveMin(
            const Expression<L> & left, const Expression<R> & right, 
            const Argument<I> & a
        ) {
            auto leftDerivative { left.derive(a) };
            auto rightDerivative { right.derive(a) };
            auto derivative {
                if_(left < right, leftDerivative)
                .else_(if_(right < left, rightDerivative)
                .else_(leftDerivative))
                
                // .else_(if_(leftDerivative == rightDerivative, leftDerivative)
                // .else_(Expression<Error>("Derivative undefined"))))
            };
            return derivative;
        }
        
        template<typename L, typename R, std::size_t I>
        inline auto deriveMax(
            const Expression<L> & left, const Expression<R> & right, 
            const Argument<I> & a
        ) {
            auto leftDerivative { left.derive(a) };
            auto rightDerivative { right.derive(a) };
            auto derivative {
                if_(left > right, leftDerivative)
                .else_(if_(right > left, rightDerivative)
                .else_(leftDerivative))
                
                // .else_(if_(leftDerivative == rightDerivative, leftDerivative)
                // .else_(Expression<Error>("Derivative undefined"))))
            };
            return derivative;
        }
    }
    
    BINARY_OPERATOR(min, std::min(left, right), deriveMin(left, right, a))
    BINARY_OPERATOR(max, std::max(left, right), deriveMax(left, right, a))
    
    // "Ternary operator"
        
    template<typename C, typename L, typename R>
    auto ifElse(
        const Expression<C> & condition,
        const Expression<L> & left,
        const Expression<R> & right
    ) {
        using T = ConditionalExpression<C, L, R>;
        return Expression<T>(T(condition, left, right));
    }
    
    template<typename C, typename L>
    auto ifElse(
        const Expression<C> & condition,
        const Expression<L> & left,
        const double right
    ) {
        return ifElse(condition, left, Expression<Constant>(right));
    }
    
    template<typename C, typename R>
    auto ifElse(
        const Expression<C> & condition,
        const double left,
        const Expression<R> & right
    ) {
        return ifElse(condition, Expression<Constant>(left), right); 
    }
    
    template<typename C>
    auto ifElse(
        const Expression<C> & condition,
        const double left,
        const double right
    ) {
        return ifElse(
            condition, Expression<Constant>(left), Expression<Constant>(right));
    }
    
    template<typename C, typename L>
    auto if_(const Expression<C> & condition, const Expression<L> & left) {
        return IfElseBuilder<C, L>(condition, left);
    }
    
    template<typename C>
    auto if_(const Expression<C> & condition, const double left) {
        return IfElseBuilder<C, Constant>(condition, Expression<Constant>(left));
    }
    
    template<typename C, typename L>
    IfElseBuilder<C, L>::IfElseBuilder(
        const Expression<C> & condition, const Expression<L> & left
    ) : condition(condition), left(left) {}
    
    template<typename C, typename L>
    template<typename R>
    auto IfElseBuilder<C, L>::else_(const Expression<R> & right) {
        return ifElse(condition, left, right);
    }
    
    template<typename C, typename L>
    auto IfElseBuilder<C, L>::else_(const double right) {
        return ifElse(condition, left, Expression<Constant>(right));
    }
}