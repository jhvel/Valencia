#ifndef PARAMETER_H
#define PARAMETER_H

#include <cstring>
#include <cmath>

#include "Unit.hpp"
#include "StringBuffer.hpp"

namespace Valencia {

    // Explicit lambdas

    #define REGISTER_PARAMETER_TYPE(type, reader, writer) \
    template<> inline const char * Parameter<type>::getType() const { \
        return #type; \
    } \
    template<> inline void Parameter<type>::readValue(const StringBuffer & buffer) { \
        reader(this, buffer); \
    } \
    template<> inline void Parameter<type>::writeValue(StringBuffer & buffer) const { \
        writer(this, buffer); \
    }

    // Inline lambdas

    #define READER_INLINE(type) [] (Parameter<type> * parameter, const StringBuffer & buffer) -> void
    #define WRITER_INLINE(type) [] (const Parameter<type> * parameter, StringBuffer & buffer) -> void

    #define REGISTER_PARAMETER_TYPE_INLINE(type, reader, writer) \
    template<> inline const char * Parameter<type>::getType() const { \
        return #type; \
    } \
    template<> inline void Parameter<type>::readValue(const StringBuffer & buffer) { \
        READER_INLINE(type) reader (this, buffer); \
    } \
    template<> inline void Parameter<type>::writeValue(StringBuffer & buffer) const { \
        WRITER_INLINE(type) writer (this, buffer); \
    }

    #define HAS_TYPE(parameter, typename) !std::strcmp(parameter->type, #typename)
    #define PARAMETER(type, name, value, unit, description) Parameter<type> name { #name, value, unit, description };

    #define THROW(format, ...) \
    throw std::runtime_error(StringBuffer().put(format, ##__VA_ARGS__).getString());

    /* ====================== */
    /* === TypedParameter === */
    /* ====================== */

    // Forward declaration of Parameter<T>
    template<typename T>
    struct Parameter;

    struct TypedParameter {
        const char * name;          // required
        const char * type;          // required
        const Unit * unit;          // may be nullptr
        const char * description;   // may be nullptr
        
        TypedParameter(
            const char * name,
            const char * type,
            const Unit * unit = nullptr,
            const char * description = nullptr
        );
        virtual ~TypedParameter() = default;
        
        virtual void readValue(const StringBuffer & buffer) = 0;
        virtual void writeValue(StringBuffer & buffer) const = 0;
        
        void read(const StringBuffer & buffer);
        void write(StringBuffer & buffer) const;
        
        template<typename T>
        auto cast() { return static_cast<Parameter<T> *>(this); }
        
        const char * getNameUnitSymbol() const { return nameUnitSymbol.getString(); }
        
        private:
            template<typename... A>
            [[ noreturn ]] void raise(const char * format, A... args) {
                // TODO: Format string for more useful error messages
                // hasError = true;   
                throw std::runtime_error(format);
            }
        
            // Generated
            StringBuffer nameUnitSymbol;
    };

    /* ================= */
    /* === Parameter === */
    /* ================= */

    template<typename T>
    struct Parameter : public TypedParameter {
        // Do not use this constructor directly, use PARAMETER macro
        Parameter(
            const char * name,
            T value, 
            const Unit * unit = nullptr,
            const char * description = nullptr
        ) : TypedParameter(name, getType(), unit, description),
            value(value), defaultValue(value)
        {   
            if (!type) {
                THROW("Parameter %s has an unregistered data type. "
                      "Did you forget to call REGISTER_PARAMETER_TYPE?", name)
            }
        }
        ~Parameter() = default;
         
        // These functions get overridden with REGISTER_PARAMETER_TYPE
        const char * getType() const { return nullptr; } 
        
        void readValue(const StringBuffer & buffer) override {
            THROW("Parameter %s (%s) has no specialized template for readValue(). "
                   "Please set read callback using REGISTER_PARAMETER_TYPE.", name, type)
        }
        
        void writeValue(StringBuffer & buffer) const override {
            THROW("Parameter %s (%s) has no specialized template for writeValue(). "
                  "Please set write callback using REGISTER_PARAMETER_TYPE.", name, type)
        }
        
        void setValue(T value) {
            this->value = value;
        }
        
        operator T() const { return value; }

        // Hm ...
        void setDefaultValue(T value) {
            this->value = value;
            this->defaultValue = value;
        }
        
        bool hasDefaultValue { true };
        T value;
        T defaultValue;
    };

    /* ===================== */
    /* === Parameterized === */
    /* ===================== */

    // Abstract base class for all classes that provide user-settable
    // parameters, e.g. Simulation, VoltageSource and Model.
    struct Parameterized {
        template<typename... P>
        Parameterized(P&... parameterPack) : count(sizeof...(P)) {
            // Must be in constructor body
            parameters = new TypedParameter *[count] { & parameterPack... };
        }
        virtual ~Parameterized();
        
        TypedParameter ** getParameters();
        std::size_t getParameterCount() const;
        
        // These functions throw exceptions if something went wrong
        void readParameters(const char * fileName);
        void writeParameters(const char * fileName);
        
        TypedParameter * findParameter(const char * name);
        TypedParameter * getParameter(const std::size_t index);
        
        template<typename T>
        Parameter<T> * findParameter(const char * name) {
            auto parameter { findParameter(name) };
            return parameter? parameter->cast<T>() : nullptr;
        }
        
        template<typename T>
        Parameter<T> * getParameter(const std::size_t & index) {
            auto parameter { getParameter(index) };
            return parameter? parameter->cast<T>() : nullptr;
        }
        
        private:
            // Fake parameter structure only used for searching
            struct ParameterSearch {
                const char * name;
                ParameterSearch(const char * search) : name(search) {}
                virtual ~ParameterSearch() = default; /* virtual destructor required! */
            };
            
            // Used to check if all parameters were present in a loaded file
            struct Checklist {
                Checklist(std::size_t size);
                ~Checklist();
                
                bool contains(std::size_t index);
                bool put(std::size_t index);
                bool isComplete() const;
                
                template<typename C>
                void forEachRemaining(C callback) {
                    for (std::size_t i { 0 }; i < size; i++) {
                        if (!list[i])
                            callback(i);
                    }
                }
                
                private:
                    bool * list {};
                    std::size_t size;
            };
            
            // Used for binary search. Do not sort original parameters
            // because this also would affect the order in which the
            // parameters are displayed in valencia_gui.
            TypedParameter ** sortedParameters {};
            TypedParameter ** parameters {};
            const std::size_t count {};
            
            bool sort();
            static int compare (const void * ptr1, const void * ptr2);
            TypedParameter * const * findParameterInternal(const char * name);
            bool findSortedParameterIndex(const char * name, std::size_t & index);
            TypedParameter * getSortedParameter(const std::size_t index);
    };

    /* Register parameter types */

    // Hide lambdas in anonymous namespace
    namespace {    
        auto readDouble = [] (
            Parameter<double> * parameter, const StringBuffer & buffer
        ) -> void {
            char * readEnd;
            const auto string { buffer.getString() };
            const auto stringSize { buffer.getStringSize() };
            const auto read { std::strtod(string, &readEnd) };
            const auto readSize { readEnd - string };
            const auto isValid {
                readSize == stringSize && std::fabs(read) != HUGE_VAL
            };
            
            if (isValid)
                parameter->value = read;
            else
                throw std::runtime_error("Invalid double parameter value");
        };
        
        auto writeDouble = [] (
            const Parameter<double> * parameter, StringBuffer & buffer
        ) -> void {
            buffer.put(parameter->value);
        };
        
        auto readString = [] (
            Parameter<std::string> * parameter, const StringBuffer & buffer
        ) -> void {
            const std::size_t size { buffer.getStringSize() };
            
            if (size >= 2 && buffer.startsWith("\"") && buffer.endsWith('\"')) {
                StringBuffer content;
                content.put(buffer, 1, size - 1);
                parameter->value = content.getString();
            } else
                throw std::runtime_error("Invalid string parameter value");
        };
        
        auto writeString = [] (
            const Parameter<std::string> * parameter, StringBuffer & buffer
        ) -> void {
            buffer.put("\"%s\"", parameter->value.c_str());
        };
        
        // ...
    }

    REGISTER_PARAMETER_TYPE_INLINE(bool,
    /* R */ { THROW("readBool() not implemented yet"); },
    /* W */ { buffer.put(parameter->value? "true" : "false"); }
    )

    REGISTER_PARAMETER_TYPE(double, readDouble, writeDouble)
    REGISTER_PARAMETER_TYPE(std::string, readString, writeString)

} // namespace Valencia

#endif