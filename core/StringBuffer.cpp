#include "StringBuffer.hpp"

#include <cstring>
#include <cmath>
#include <new>
#include "Utils.hpp"

using namespace Valencia;

StringBuffer::StringBuffer() : 
    StringBuffer(DEFAULT_INITIAL_SIZE, DEFAULT_MAX_SIZE) {}
    
StringBuffer::StringBuffer(std::size_t initialSize, std::size_t maxSize) : 
    size(initialSize), maxSize(maxSize) {
    
    init();
}

void StringBuffer::init() {
    buffer = new (std::nothrow) char[size];
    
    if (buffer)
        putTerminator();        
    else
        raise("Error while initializing buffer");
}

StringBuffer::~StringBuffer() {
    delete [] buffer;
}

bool StringBuffer::grow() {
    const std::size_t newSize { size * 2 };     
    if (newSize <= maxSize) {
        char * newBuffer { new (std::nothrow) char[newSize] };
        if (newBuffer) {
            if (seek > 0)
                std::strncpy(newBuffer, buffer, seek + 1); /* include terminator */
            
            delete [] buffer;
            buffer = newBuffer;
            size = newSize;
            return true;
        }
    }
    return false;
}

StringBuffer & StringBuffer::putInt(const int value) {
    return putStringFormatted("%d", value);
}

StringBuffer & StringBuffer::putChar(const char c) {
    if (getRemainingSize() > 1) {
        *get() = c;
        seekBy(1);
        putTerminator();
        return *this;
    } else if (grow())
        return putChar(c);
    
    raise("Error in putChar()");
}

StringBuffer & StringBuffer::putString(const char * string) {
    return putString(string, string? std::strlen(string) : 0);
}

StringBuffer & StringBuffer::putString(
    const char * string, const std::size_t & size
) {   
    if (string) {
        const std::size_t stringSize { std::strlen(string) };
        if (size <= stringSize) {
            if (getRemainingSize() > size) {
                std::strncpy(get(), string, size + 1);
                seekBy(size);
                putTerminator(); /* Do not assume string has a terminator */ 
                return *this;
            } else if (grow())
                return putString(string, size);
        }
    }    
    raise("Error in putString()");
}


StringBuffer & StringBuffer::putCharRepeated(const char c, int n) {
    for (int i = 0; i < n; i++)
        putChar(c);
    return *this;
}

void StringBuffer::putTerminator() {
    // Seek must never be >= totalSize
    if (seek >= getTotalSize())
        raise("Seek invalid\n");
    
    *get() = '\0';
}

StringBuffer & StringBuffer::putDouble(
    const double & value,
    const bool trimDecimals
    // ...
) {
    static const int significantDigits = 12;
    
    // Prechecks
    if (std::isnan(value))
        return putString("nan");
    
    if (std::isinf(value))
        return putString(value > 0? "inf" : "-inf");

    if (value == 0.0)
        return putString("0");
    
    // Parse value
    const int exponent { Utils::getExponent(value) };
    const int exponent3 { Utils::toStride3Exponent(exponent) }; // Modify exponent
    const double mantissa { value / std::pow(10, exponent3) };
    int decimalCount { significantDigits - 1 + exponent3 - exponent };
    
    if (decimalCount < 1)
        decimalCount = 1;
    
    // create format string
    char format[10]; // Safe
    std::snprintf(format, 10, "%%.%df", decimalCount);
     
    // write mantissa
    putStringFormatted(format, mantissa);
        
    // trim zeros (and decimal point)
    if (trimDecimals) {
         trim('0'); trim('.');
    }

    // Write exponent and/or cut off trailing zeros
    if (exponent3 != 0)
        putStringFormatted("e%d", exponent3);
    
    return *this;
}

bool StringBuffer::putFileLine(std::FILE * file) {
    if (file) {
        while (std::fgets(get(), getRemainingSize(), file)) {
            seekToEnd(); // Safe, fgets always adds terminator
            
            if (std::feof(file) || endsWith('\n')) {
                trim('\n'); // Remove line feed, if there is one
                return true; 
            } else if (!grow())
                raise("Can not grow buffer while reading file\n");
        }
    }
    return false;
}

StringBuffer & StringBuffer::putBuffer(const StringBuffer & other) {
    return putBuffer(other, 0, other.getStringSize());
}

StringBuffer & StringBuffer::putBuffer(
    const StringBuffer & other,
    const std::size_t & start,
    const std::size_t & end
) {
    if (start <= end && end <= other.getStringSize())
        return putString(other.getString() + start, end - start);
    else
        raise("Invalid range for copying buffer");
}

StringBuffer & StringBuffer::put(
    const StringBuffer & other,
    const std::size_t & start,
    const std::size_t & end
) {
    return putBuffer(other, start, end);
}

StringBuffer & StringBuffer::trim(const char c) {
    while (endsWith(c))
        seekBy(-1);
    putTerminator();
    return *this;
}

StringBuffer & StringBuffer::trimWhiteSpace() {
    while (endsWithWhiteSpace())
        seekBy(-1);
    putTerminator();
    return *this;
}

bool StringBuffer::find(
    const char c, std::size_t & index, const std::size_t offset) const {
    if (offset < seek) {
        const auto stringPtr { getString() + offset };
        const auto charPtr { std::strchr(stringPtr, c) };
        if (charPtr && charPtr >= stringPtr) {
            index = charPtr - stringPtr + offset;
            return true;
        }
    }
    return false;
}

StringBuffer & StringBuffer::replaceAll(const char c, const char by) {
    std::size_t index { 0 };
    std::size_t start { 0 };
    
    while (find(c, index, start)) {
        buffer[index] = by;
        start = index + 1;
    }
    return *this;
}

bool StringBuffer::endsWith(const char c) const {
    return seek > 0 && buffer[seek - 1] == c;
}

bool StringBuffer::endsWithWhiteSpace() const {
    return seek > 0 && std::isspace(buffer[seek - 1]);
}

bool StringBuffer::startsWith(const char * start) const {
    const std::size_t startSize { std::strlen(start) };
    return (getStringSize() >= startSize) && 
        !std::strncmp(getString(), start, startSize);
}

// Use carefully
void StringBuffer::seekBy(int by) {
    this->seek += by;
}

// Make sure there is a terminator!
void StringBuffer::seekToEnd() {
    seek = getStringSize();
}

char * StringBuffer::get() const {
    return buffer + seek;
}

const char * StringBuffer::getString() const {
    return buffer;
}

const char  StringBuffer::getChar(std::size_t index) const {
    return index < seek? buffer[index] : '\0';buffer;
}

std::size_t StringBuffer::getRemainingSize() const {
    return size - seek;
}

std::size_t StringBuffer::getTotalSize() const {
    return size;
}

std::size_t StringBuffer::getStringSize() const {
    return std::strlen(getString());
}

bool StringBuffer::isEmpty() const {
    return seek == 0;
}

std::size_t StringBuffer::fitRangeSize(
    const std::size_t start, const std::size_t end
) {
    if (start > end)
        raise("Range start must be larger than end");
    
    std::size_t size { end - start + 1 }; /* Include terminator */
    
    if (size <= DEFAULT_INITIAL_SIZE)
        return DEFAULT_INITIAL_SIZE;
    else
        return Utils::bitCeil(size);
}

void StringBuffer::reset() {
    seek = 0;
    putTerminator();
}

/* ================= */
/* === Tokenizer === */
/* ================= */

bool Tokenizer::seekStart(const StringBuffer & buffer, std::size_t & start, 
                          const std::size_t & end
) {
    while(start < end && std::isspace(buffer.getChar(start))) start++;
    return start != end;
} 
