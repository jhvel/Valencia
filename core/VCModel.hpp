#ifndef V_MODEL_H
#define V_MODEL_H

#include "Model.hpp"
#include "Parameter.hpp"
#include "Unit.hpp"

#include "Expression.hpp"
#include "NewtonsMethod.hpp"

namespace Valencia {
    
    class VCModelOutput;
    
    // TODO: Fix public/private access
    namespace UsingOperator { class VCModel : public Model {
        
        friend class VCModelOutput;
        
        #define EXPORT_PARAMETER(NAME, VALUE, UNIT, DESCRIPTION)         \
        Parameter<double> _ ## NAME { #NAME, VALUE, UNIT, DESCRIPTION }; \
        Param NAME { _ ## NAME.value };
        
        #define EQ_PARAMETER(NAME, VALUE) \
        double _ ## NAME { VALUE };       \
        Param NAME { _ ## NAME };
        
        #define EQUATION(NAME, VALUE) decltype(VALUE) NAME { VALUE };
        
        template<std::size_t N>
        using Argument = Expression::ArgumentExpression<N>;
        
        using Param = Expression::ParameterExpression;
        
        /* Constants */
        static constexpr double e26   { 1e26 };
        
        /* "Special" units */
        Unit perKelvin_u { "Per Kelvin",                        "K⁻¹",                  "Temperature coefficient" };
        Unit mobility_u  { "Square Metre per Volt and Second",  "m²\u2009(V\u2009s)⁻¹", "Electron mobility" };
        Unit Ws_per_K_u  { "Watt Second per Kelvin",            "W\u2009s\u2009K⁻¹",    "Thermal Capacitance" };

        /* Parameters */
        
        PARAMETER(std::string, name,    "",       nullptr,            "Name of this parameter set")
        
        EXPORT_PARAMETER(T_0,        293.15,      &kelvin_u,          "Ambient temperature")
        EXPORT_PARAMETER(alpha_line,   3.92e-3,   &perKelvin_u,       "Temperature coefficient of the lines")
        EXPORT_PARAMETER(R_th,        16e6,       &kelvinPerWatt_u,   "Thermal resistance")
        EXPORT_PARAMETER(R_th_line,   90.47147e3, &kelvinPerWatt_u,   "Thermal resistance of the lines")
        EXPORT_PARAMETER(R_line0,    719.2437,    &ohm_u,             "Line resistance at I = 0 A")
        EXPORT_PARAMETER(R_ICL,      650,         &ohm_u,             "Series resistance of the ICL layer")
        EXPORT_PARAMETER(l_cell,       3.0e-9,    &metre_u,           "Total length of cell")
        EXPORT_PARAMETER(l_diff,       0.5e-9,    &metre_u,           "Length of concentration gradient")
        EXPORT_PARAMETER(a,            0.25e-9,   &metre_u,           "Ion hopping distance") 
        EXPORT_PARAMETER(mu_n,         4e-6,      &mobility_u,        "Electron mobility")
        EXPORT_PARAMETER(enableI_diff, 1,         nullptr,            "Enable ion diffusion current")
        EXPORT_PARAMETER(m_factor,     1,         nullptr,            "Effective mass factor (electrons)")
        EXPORT_PARAMETER(z,            2,         nullptr,            "Oxygen vacancy charge number")
        
        // Changed for VCModel
        EXPORT_PARAMETER(r,           20e-9,       &metre_u,           "Radius of disc/plug")
        EXPORT_PARAMETER(nu_0,         2e10,       &hertz_u,           "Attempt frequenzy")
        EXPORT_PARAMETER(dE_0,         1,          &eV_u,              "Activation energy for ion migration")
        EXPORT_PARAMETER(N_plug,      100,         &perCubicMetre_u,   "Oxgen vancancy concentration in the plug")
        EXPORT_PARAMETER(N_max,       100,         &perCubicMetre_u,   "Maximum Oxygen vacancy concentration in the disc")
        EXPORT_PARAMETER(N_min,        10,         &perCubicMetre_u,   "Minimum Oxygen vacancy concentration in the disc")
        EXPORT_PARAMETER(N_disc0,      10,         &perCubicMetre_u,   "Initial Oxygen vacancy concentration in the disc")
        EXPORT_PARAMETER(C_th,         0,          &Ws_per_K_u,        "Thermal capacitance")
        
        // New in VCModel
        EXPORT_PARAMETER(eps_r,        8,         &Ws_per_K_u,        "Relative permittivity")
        EXPORT_PARAMETER(phi,          0.9,       &eV_u,              "Lower conduction band edge minus Fermi level")
        EXPORT_PARAMETER(phi_B0,       3.5,       &eV_u,              "Barrier height relativ to metal Fermi level")
        EXPORT_PARAMETER(phi_subband,  0.5,       &eV_u,              "Width of oxygen vacancy subband directly below the semiconductor Fermi level")
        
        /* State */
        
        using State = Array<double, 4>; /* I, V, N_disc, V_simmons */
        
        EQ_PARAMETER( limitCurrent, 0 )
        
        template<typename... A>
        struct StateInternal {
            EQ_PARAMETER( V_src, 0 )
            EQ_PARAMETER( I_max, 0 )
            
            EQ_PARAMETER( dt, 0 )
            EQ_PARAMETER( dN_disc, 0 )
            EQ_PARAMETER( dVdt, 0 )
            
            EQ_PARAMETER( T, 0 )
            EQ_PARAMETER( P_th, 0 )

            Expression::VariableAdapter<A...> adapter;
            Param I, V, N_disc, V_simmons;
           
            StateInternal(const std::tuple<A...> & arguments) 
                : adapter { arguments },
                  I { adapter.values[0] },
                  V { adapter.values[1] },
                  N_disc { adapter.values[2] },
                  V_simmons { adapter.values[3] } {}
        };
        
        public:
        
        /* Arguments */
        
        const Argument<0> I;
        const Argument<1> V;
        const Argument<2> N_disc;
        const Argument<3> V_simmons;
        
        /* Internal state */
        
        StateInternal<Argument<0>, Argument<1>, Argument<2>, Argument<3>> state { 
            Model::wrapArguments(I, V, N_disc, V_simmons) 
        };
        
        /* Equations */
        
        public:
        const EQUATION( A, pi * O::pow(r, 2) )
        const EQUATION( R_series, R_ICL + R_line0 * (1 + R_line0 * alpha_line * R_th_line * I * I) )
        const EQUATION( V_cell, V - R_series * I )
        
        // Temperture -----
        
        // Equation for dT/dt has been approximated using trapezoidal rule and
        // was then solved for T_i = ...
        const EQUATION( P_th, I * V_cell )
        const EQUATION( ft, 0.5 * state.dt / (C_th * R_th) )
        const EQUATION( T_Cth,  (state.T * (1 - ft) + 0.5 * state.dt / C_th * (state.P_th + P_th)) / (1 + ft) )
        const EQUATION( T_Rth,  P_th * R_th )
        const EQUATION( T,  O::ifElse(C_th == 0, T_Rth, T_Cth) )
        
        // General
        const EQUATION( k_BT, k_B * (T + T_0) )
        
        // Simmons formula (current) -------
        
        const EQUATION( phi_builtin, phi_B0 - phi - V_simmons )

        // Depletion distance and tunneling distance
        const EQUATION( l_depletion, O::sqrt(2 * eps_r * eps_0 / (z * e * N_disc * e26) * phi_builtin) )
        const EQUATION( l_tunnel,    l_depletion * (1 - O::sqrt(O::ifElse(V_simmons >= 0, phi_subband, phi_subband - V_simmons) / phi_builtin)) )
        
        // Potential (relative to metal Fermi level)
        const EQUATION( pot_metal, e * phi_B0 )
        const EQUATION( pot_oxide, e * phi + e * phi_subband + O::ifElse(V_simmons >= 0, e * V_simmons, 0) )
        const EQUATION( pot_mean, 0.5 * (pot_metal + pot_oxide) )

        // Prefactors
        const EQUATION( A_simmons, 4 * pi * l_tunnel * O::sqrt(2 * m_e * m_factor) / h )
        const EQUATION( B_simmons, e / (2 * pi * h * l_tunnel * l_tunnel) )
                
        // Simmons 
        const EQUATION( I_simmons0, pot_mean * O::exp(-A_simmons * O::sqrt(pot_mean)) )
        const EQUATION( I_simmons1,            O::exp(-A_simmons * O::sqrt(pot_mean - e * V_simmons)) )
        const EQUATION( I_simmons, 
            -B_simmons * A * (I_simmons0 - (pot_mean - e * V_simmons) * I_simmons1)
        )
        
        // Corrected
        const EQUATION( V_simmons_fromI, 
            (pot_mean - I_simmons0 / I_simmons1 - I / (B_simmons * A * I_simmons1)) / e
        )
            
        // N_disc -----------------
        
        const EQUATION( E_ion, V_simmons / l_depletion );
        
        const EQUATION( gamma, z * a * E_ion / (pi * dE_0) )
        
        const EQUATION( dE_common, O::sqrt(1 - gamma * gamma) + gamma * O::asin(gamma) )
        const EQUATION( dE_f,    dE_0 * e * (dE_common - pi / 2 * gamma) ) // Forward
        const EQUATION( dE_r,    dE_0 * e * (dE_common + pi / 2 * gamma) ) // Reverse
        
        const EQUATION( exp_f,  O::exp(-dE_f / k_BT) )
        const EQUATION( exp_r,  O::exp(-dE_r / k_BT) )
        const EQUATION( J_ion_common, nu_0 * a * z * e ) // Is a double, not an expression
        
        const EQUATION( c_V_O, 0.5 * (N_plug + N_disc) * e26 ) // Arithmetic mean
        const EQUATION( F_limit, 1 - O::ifElse(V > 0, N_min / N_disc, N_disc / N_max) ) // Kein Exponent 10, der ist nicht begründbar
        const EQUATION( gradN, (N_plug - N_disc) * e26 / l_diff )
        
        const EQUATION( J_drift, c_V_O * F_limit * J_ion_common * (exp_f - exp_r) )
        const EQUATION( J_diff, -1 * a / 2 * gradN * J_ion_common * (exp_f + exp_r) )
        const EQUATION( J_ion, J_drift + enableI_diff * J_diff )
        
        const EQUATION( dN_disc, -J_ion / (z * e * l_depletion) )
        
        // Used for results:
        const EQUATION( I_ion,       A * J_ion )
        const EQUATION( I_ion_drift, A * J_drift )
        const EQUATION( I_ion_diff,  A * enableI_diff * J_diff )
        const EQUATION( J_ion_diff,  enableI_diff * J_diff )
        
        // Other
        
        const EQUATION( R_plug, (l_cell - l_depletion) / (A * e * z * mu_n * N_plug * e26) )
        
        const EQUATION( V_plug, I * R_plug )
        const EQUATION( V_series, I * R_series )
        const EQUATION( V_simmons_fromKirchhoff, V - V_plug - V_series )
        
        // These are the main functions to be solved
        // In residual form
        
        const EQUATION( f_I, O::ifElse(limitCurrent, state.I_max, I_simmons) - I )
        const EQUATION( f_V, O::ifElse(limitCurrent, V_simmons + V_plug + V_series, state.V_src) - V )

        const EQUATION( f_N_disc, O::min(N_max, state.N_disc + 0.5 * (state.dN_disc + dN_disc) / e26 * state.dt) - N_disc )        
        const EQUATION( f_V_simmons, O::ifElse(limitCurrent, V_simmons_fromI, V_simmons_fromKirchhoff) - V_simmons )
        
        // Experimental
        const EQUATION( dIdt, I_simmons.derive(N_disc) * dN_disc + I_simmons.derive(V_simmons) * state.dVdt )
        const EQUATION( dIdt_V, I_simmons.derive(V_simmons) * state.dVdt )
        

        /* Functions */
        
        
        void setCurrentComplianceEnabled(const bool enabled) {
            limitCurrent = enabled;
        }
    
        VCModel();
        
        auto getEquations() const {
            return Model::wrapEquations(f_I, f_V, f_N_disc, f_V_simmons);
        }
        
        auto getArguments() const {
            return Model::wrapArguments(I, V, N_disc, V_simmons);
        }
        
        auto getStateAsVariables() const {
            return state.adapter.variables;
        }
        
        double getdN_disc() const {
            return state.dN_disc() / e26;
        }
        
        double getN_disc() const {
            return state.N_disc();
        }
        
        double getCurrent() const {
            return state.I();
        }
        
        State getInitialState() const;
        State getState() const;
        
        void setState(const State & state);
        void resetState();
        
        void setVoltage(const double V, const double dt);
        void setVoltageRateOfChange(const double dUdt);
        void setCurrent(const double I, const double dt);
        
        ModelOutput * createOutput(const std::size_t maxSize) const override;
        
    }; }
    
    
    class VCModelOutput : public ModelOutput {

        #define VFROM(ACTION)                                                    \
        [](double * data, const SimulationState * state, const Model * model) {  \
            const auto m { static_cast<const UsingOperator::VCModel *>(model) }; \
            const auto variables { m->getStateAsVariables() };                   \
            data[state->i] = ACTION;                                             \
        }
    
        // Units
        Unit perCMAndS_u { "Per Cubic Metre and Second",       "m⁻³\u2009s⁻¹",         "Density Rate of Change" };
        
         // Model results
        RESULT(U,           volt_u,          VFROM( m->V(variables)            )) 
        RESULT(N_disc,      perCubicMetre_u, VFROM( m->state.N_disc()          ))
        RESULT(I,           ampere_u,        VFROM( m->I(variables)            ))
        RESULT(U_simmons,   volt_u,          VFROM( m->state.V_simmons()       ))
        RESULT(T,           kelvin_u,        VFROM( m->state.T()               ))
        RESULT(l_depletion, metre_u,         VFROM( m->l_depletion(variables)  ))
        RESULT(l_tunnel,    metre_u,         VFROM( m->l_tunnel(variables)     ))
        RESULT(dN_discdt,   perCMAndS_u,     VFROM( m->state.dN_disc()         )) 
        RESULT(dIdt,        perCMAndS_u,     VFROM( m->dIdt(variables)         )) 
        RESULT(dIdt_V,      perCMAndS_u,     VFROM( m->dIdt_V(variables)       )) 
        RESULT(dVdt,        perCMAndS_u,     VFROM( m->state.dVdt()            )) 
        RESULT(absI,        ampere_u,        VFROM( std::fabs(m->I(variables)) ))
        RESULT(I_ion,       ampere_u,        VFROM( m->I_ion(variables)        ))
        RESULT(I_ion_drift, ampere_u,        VFROM( m->I_ion_drift(variables)  ))
        RESULT(I_ion_diff,  ampere_u,        VFROM( m->I_ion_diff(variables)   ))
        RESULT(U_plug,      volt_u,          VFROM( m->V_plug(variables)       ))
        RESULT(U_series,    volt_u,          VFROM( m->V_series(variables)     ))
        RESULT(U_cell,      volt_u,          VFROM( m->V_cell(variables)       ))
          
        public:
            VCModelOutput(const std::size_t maxSize = 10);
    };
}

#endif