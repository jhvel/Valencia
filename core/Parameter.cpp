#include "Parameter.hpp"
#include "File.hpp"

#include <new>

using namespace Valencia;

/* ====================== */
/* === TypedParameter === */
/* ====================== */

TypedParameter::TypedParameter(
    const char * name,
    const char * type,
    const Unit * unit,
    const char * description
) : name(name), type(type), unit(unit), description(description) {
    nameUnitSymbol.put(unit? "%s [%s]" : "%s", name, unit? unit->symbol : nullptr);
}

void TypedParameter::read(const StringBuffer & buffer) {
    constexpr std::size_t count { 5 };
    std::size_t i { 0 };
    StringBuffer tokens[count] {};
    
    Tokenizer::tokenize(buffer, '\t', [&tokens, &i] (
        const StringBuffer & buffer,
        std::size_t start,
        std::size_t end
    ) -> bool {
        tokens[i++].put(buffer, start, end).trimWhiteSpace();
        return i < count;
    }, true);
    
    /* Reading tokens */
    
    // name
    const char * name = tokens[0].getString();
    if (std::strcmp(this->name, name))
        THROW("Name does not match");
    
    // type
    const char * type = tokens[1].getString();
    if (std::strcmp(this->type, type))
        THROW("type does not match");
    
    
    // value
    readValue(tokens[2]); // throws
    
    // TODO: Read optional information?
    
    // unit
    // ...
    
    // description
    // ...
}

void TypedParameter::write(StringBuffer & buffer) const {
    buffer.put(name).put('\t').put(type).put('\t');
    writeValue(buffer);
    
    if (unit)
        buffer.put('\t').put(unit->symbol);
    
    if (description)
        buffer.put('\t').put(description);
}

/* ===================== */
/* === Parameterized === */
/* ===================== */

TypedParameter * Parameterized::getParameter(const std::size_t index) {
    return index < getParameterCount()?
        parameters[index] : nullptr;
}

TypedParameter * Parameterized::getSortedParameter(const std::size_t index) {
    if (!sortedParameters && !sort())
        throw std::runtime_error("Could not sort parameters");
    
    return index < getParameterCount()?
        sortedParameters[index] : nullptr;
}

TypedParameter * const * Parameterized::findParameterInternal(const char * name) {
    if (!sortedParameters && !sort())
        throw std::runtime_error("Could not sort parameters, unable to search for parameter");
    
    const ParameterSearch search(name);
    const auto searchPtr { reinterpret_cast<const TypedParameter *>(&search) };
    const auto resultPtr { static_cast<TypedParameter * const *>(
        std::bsearch(&searchPtr, sortedParameters, count, 
            sizeof(TypedParameter *), compare)
    )};
    
    return resultPtr;
}

TypedParameter * Parameterized::findParameter(const char * name) {
    const auto resultPtr { findParameterInternal(name) };
    return resultPtr? *resultPtr : nullptr;
}

bool Parameterized::findSortedParameterIndex(const char * name, std::size_t & index) {
    const auto resultPtr { findParameterInternal(name) };
    if (resultPtr) {
        index = resultPtr - sortedParameters;
        return true;
    }
    return false;
}

bool Parameterized::sort() {
    if (sortedParameters)
        return true;
    
    sortedParameters = new (std::nothrow) TypedParameter *[count];
    if (sortedParameters) {
        for (std::size_t i { 0 }; i < count; i++) 
            sortedParameters[i] = parameters[i];
        
        std::qsort(sortedParameters, count, sizeof(TypedParameter *), compare);
        return true;
    }
    return false;
}

int Parameterized::compare(const void * ptr1, const void * ptr2) {
    const auto parameter1 { *static_cast<TypedParameter * const *>(ptr1) };
    const auto parameter2 { *static_cast<TypedParameter * const *>(ptr2) };
    
    const int result { std::strcmp(parameter1->name, parameter2->name) };
    // printf_s("%s vs. %s = %d\n", parameter1->name, parameter2->name, result);
    return result;
} 

Parameterized::~Parameterized() {
    delete [] sortedParameters;
    delete [] parameters;
}

TypedParameter ** Parameterized::getParameters() {
    return parameters;
}

std::size_t Parameterized::getParameterCount() const {
    return count;
}

// Reading/writing parameters from/to files
// This needs to be bullet-proof because hidden loss of data or even data corruption is a nightmare

// throws std::runtime_error
// TODO: Print more specific error messages
void Parameterized::readParameters(const char * fileName) {
    Checklist checklist(count);
    File file(fileName, File::Mode_Read);
    StringBuffer buffer;
    std::size_t index { 0 };
    
    // Read file line-by-line
    while (buffer.putFileLine(file)) {
        Tokenizer::tokenize(buffer, '\t', [
            this,
            &checklist,
            &index
        ] (
            const StringBuffer & buffer,
            std::size_t start,
            std::size_t end
        ) -> bool {
            // Only read non-empty token(s)
            StringBuffer token;
            token.put(buffer, start, end).trimWhiteSpace();
            
            if (token.startsWith("#"))
                return false; // Skip comments
            
            const auto name { token.getString() };
            const auto found { findSortedParameterIndex(name, index) };
            
            if (found && !checklist.contains(index)) {
                getSortedParameter(index)->read(buffer); // throws
                checklist.put(index);
            } else
                throw std::runtime_error("Line does not start with a param name"); // completely whitespace line is considered valid
            
            return false; // Only read first token
        }, true);
        buffer.reset();
    }
    
    if (!std::feof(file))
        throw std::runtime_error("Unknown error while reading file\n");
    
    if (!checklist.isComplete()) {
        // TODO: Output to error
        printf_s("Not all parameters were read. Missing:\n");
        
        checklist.forEachRemaining([this] (const std::size_t & index) {
            printf_s("%s\n", getSortedParameter(index)->name);
        });
        
        throw std::runtime_error("Not all parameters were read\n");
    }
}

// throws std::runtime_error
void Parameterized::writeParameters(const char * fileName) {
    const auto parameters { getParameters() };
    const auto count { getParameterCount() };
    
    File file(fileName, File::Mode_Write);
    FileBuffer buffer(file);
    
    for (std::size_t i { 0 }; i < count; i++) {
        parameters[i]->write(buffer);
        buffer.put('\n');
    }
}

/* Checklist */ 

Parameterized::Checklist::Checklist(std::size_t size) : 
    size(size), list(new bool[size]{}) {}

Parameterized::Checklist::~Checklist() {
    delete [] list;
}

bool Parameterized::Checklist::contains(std::size_t index) {
    if (index > size - 1)
        throw std::runtime_error("Invalid index in checklist");
    
    return list[index];
}

bool Parameterized::Checklist::put(std::size_t index) {
    if (index > size - 1)
        throw std::runtime_error("Invalid index in checklist");
    
    if (list[index])
        return false;
    
    list[index] = true;
    return true;
}

bool Parameterized::Checklist::isComplete() const {
    for (std::size_t i { 0 }; i < size; i++) {
        if (!list[i])
            return false;
    }
    return true;
}