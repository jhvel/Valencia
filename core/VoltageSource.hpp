#ifndef VOLTAGE_SOURCE_H
#define VOLTAGE_SOURCE_H

#include "Parameter.hpp"

namespace Valencia {

    /* Pure virtual interface for voltage sources */
    class VoltageSource : public Parameterized {
        public:
            template<typename... P>
            VoltageSource(P&... parameters) : Parameterized(parameters...) {}
            virtual ~VoltageSource() = default;
            
            
        
            virtual double getVoltage(double t) const = 0;
            // virtual double getVoltageRateOfChange(double t) const = 0;
            virtual double getNextCriticalTime(double t) const = 0;
            virtual bool isFinished(const double t) const = 0;
    };

    class VoltageSweep : public VoltageSource {
        public:
            VoltageSweep(float U_max = 0, float t_max = 0);
            ~VoltageSweep() = default;
        
            double getVoltage(double t) const override;
            // double getVoltageRateOfChange(double t) const override;
            double getNextCriticalTime(double t) const override;
            bool isFinished(const double t) const override;

            double getMaxVoltage() const;
            double getDuration() const;
            
        private: 
            // Parameters
            PARAMETER(std::string, name, "", nullptr, "Name of this parameter set")
            PARAMETER(double, U_max, 0, &volt_u,      "Peak voltage" )
            PARAMETER(double, t_max, 0, &second_u,    "Total voltage source time")
    };

    class VoltagePulse : public VoltageSource {
        public:
            VoltagePulse(float U_max = 0, float t_max = 0);
            ~VoltagePulse() = default;
        
            double getVoltage(double t) const override;
            // double getVoltageRateOfChange(double t) const override;
            double getNextCriticalTime(double t) const override;
            bool isFinished(const double t) const override;
            
            double getMaxVoltage() const;
            double getDuration() const;
            
        private:
            // Parameters
            PARAMETER(std::string, name, "",    nullptr,   "Name of this parameter set")
            PARAMETER(double, U_max,    0,      &volt_u,   "Peak voltage")
            PARAMETER(double, dt_raise, 1e-9,   &second_u, "Voltage raise duration")
            PARAMETER(double, dt_hold,  1e-3,   &second_u, "Duration for which the peak voltage is held")
            PARAMETER(double, dt_lower, 1e-9,   &second_u, "Voltage lower duration")
            PARAMETER(double, dt_idle,  1e-3,   &second_u, "Total voltage source time")
            PARAMETER(double, dt_slope, 1e-13,  &second_u, "Hint for time step calculation")
    };
    
    class VoltageStep : public VoltageSource {
        public:
            VoltageStep(float U_max = 0, float t_max = 0);
            ~VoltageStep() = default;
        
            double getVoltage(double t) const override;
            // double getVoltageRateOfChange(double t) const override;
            double getNextCriticalTime(double t) const override;
            bool isFinished(const double t) const override;
            
            double getMaxVoltage() const;
            double getDuration() const;
            
        private:
            // Parameters
            PARAMETER(std::string, name, "",    nullptr,   "Name of this parameter set")
            PARAMETER(double, U_max,    0,      &volt_u,   "Peak voltage")
            PARAMETER(double, dt_raise, 1e-9,   &second_u, "Voltage raise duration")
            PARAMETER(double, t_max,    1e-3,   &second_u, "Maximum duration")
            PARAMETER(double, dt_slope, 1e-13,  &second_u, "Hint for time step calculation")
    };

} // namespace Valencia

#endif