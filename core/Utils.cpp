#include "Utils.hpp"

#include <cmath>
#include <cstring>
#include <cstdio>
// #include <cassert>


using namespace Valencia;

// legacy function
// const char * StringHelper::toSuperscript(const int number) {
    // static const char * supM = u8"\u207B";
    // static const char * sup0 = u8"\u2070";
    // static const char * sup1 = u8"\u00B9";
    // static const char * sup2 = u8"\u00B2";
    // static const char * sup3 = u8"\u00B3";
    // static const char * sup4 = u8"\u2074";
    // static const char * sup5 = u8"\u2075";
    // static const char * sup6 = u8"\u2076";
    // static const char * sup7 = u8"\u2077";
    // static const char * sup8 = u8"\u2078";
    // static const char * sup9 = u8"\u2079";

    // if (std::abs(number) < 1000) {
        // const int size = 5;
        // char * buffer = new char[size];
        // std::snprintf(buffer, size, "%d", number);
        
        // char * result = new char[5 * size];
        // int j = 0; // index in result
        
        // // Repace ordinary chars with superscript ones
        // for (int i = 0; i < size; i++) {
            // switch(buffer[i]) {
                // case '-': strncpy(&result[j], supM, strlen(supM)); j += strlen(supM); break;
                // case '0': strncpy(&result[j], sup0, strlen(sup0)); j += strlen(sup0); break;
                // case '1': strncpy(&result[j], sup1, strlen(sup1)); j += strlen(sup1); break;
                // case '2': strncpy(&result[j], sup2, strlen(sup2)); j += strlen(sup2); break;
                // case '3': strncpy(&result[j], sup3, strlen(sup3)); j += strlen(sup3); break;
                // case '4': strncpy(&result[j], sup4, strlen(sup4)); j += strlen(sup4); break;
                // case '5': strncpy(&result[j], sup5, strlen(sup5)); j += strlen(sup5); break;
                // case '6': strncpy(&result[j], sup6, strlen(sup6)); j += strlen(sup6); break;
                // case '7': strncpy(&result[j], sup7, strlen(sup7)); j += strlen(sup7); break;
                // case '8': strncpy(&result[j], sup8, strlen(sup8)); j += strlen(sup8); break;
                // case '9': strncpy(&result[j], sup9, strlen(sup9)); j += strlen(sup9); break;
                // case '\0': result[j] = '\0'; delete [] buffer; return result;
            // }
        // }
        
        // // Something went wrong
        // delete [] result;
        // delete [] buffer;
    // }
    
    // return nullptr;
// }

const char * StringHelper::toPrefix(const int exponent) {
    static const int stepCount = 7;
    static const char * prefixes[2 * stepCount + 1] = {
        "z", "a", "f", "p", "n", "µ", "m", "", "k", "M", "G", "T", "P", "E", "Z"
    };
    
    if (exponent % 3 == 0 && std::fabs(exponent) >= 3 && std::fabs(exponent) <= 3 * stepCount) {
        return prefixes[stepCount + exponent / 3];
    } else
        return "?";
}

double StringHelper::readDouble(const char * buffer) {
    // Read double from buffer
    const double value = buffer? std::strtod(buffer, nullptr) : 0.0;
    return std::fabs(value) == HUGE_VAL? 0.0 : value; // TODO Output warning?
}


// Math functions (to be moved)

int Utils::getExponent(const double & value, int stride) {
    static constexpr int MIN_EXPONENT = -300;
    const double absValue = std::fabs(value);
    int exponent = 0;
    
    if (absValue >= std::pow(10, MIN_EXPONENT)) {
        if (absValue < 1)
            for (; absValue < std::pow(10, exponent); exponent -= stride);
        else
            for (; absValue >= std::pow(10, exponent + stride); exponent += stride);
    }
    return exponent;
}

int Utils::sign(const double & value) {
    return (int) ((value > 0.0) - (value < 0.0)); 
}

int Utils::toStride3Exponent(const int exponent) {
     if (exponent > 0 && exponent < 3)
        return 0;
    else if (exponent % 3 != 0)
        return exponent + 2 * (exponent % 3) - 3 * sign(exponent);
    else
        return exponent;
}

// bool Utils::contains(std::vector<const char *> & vector, const char * string) {
    // for (const char * entry : vector) {
        // if (!std::strcmp(entry, string))
            // return true;
    // }
    // return false;
// }


// TODO: Do range checks
double Utils::interpolate(const double * data, const double & x) {
    
    
    const int x0 { static_cast<int>(std::ceil(x)) };
    const int x1 { static_cast<int>(std::floor(x)) };
    
    if (x0 == x1)
        return data[x1];
    else {
        const double y0 { data[x0] };
        const double y1 { data[x1] };
        return (y0 * (x1 - x) + y1 * (x - x0)) / (x1 - x0);  
    }
}