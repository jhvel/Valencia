#include "Binding.h"

#include <pybind11/functional.h>

#include "VoltageSource.hpp"
#include "VCModel.hpp"
#include "Simulation.hpp"

using namespace Valencia;

// TODO:
// - Fix const char * parameters - done
// - Generate optional constructor arguments for parameterized
// - Ensure return_value_policy is correct
// - Clean up code
// - Add module docstrings
// - Display units?

#define BIND_CLASS(name, T, ...) auto name ## _py { py::class_<T, ##__VA_ARGS__>(m, #name) };

#define BIND_PARAMETERIZED(name, T, ...) \
BIND_CLASS(name, T, ##__VA_ARGS__) \
bindParameters(name ## _py, #name);

PYBIND11_MODULE(valencia, m) {
    // TODO: Add documentation
    m.doc() = "Python bindings for valencia_core";
    
    // Interfaces which do not provide a constructor
    BIND_CLASS(Parameterized, Parameterized)
    
    // Setter/getter overloads for Parameter<double>
    Parameterized_py.def("set_parameter", [] (Parameterized & self, const std::string & name, const double value) -> void {
        auto parameter { self.findParameter<double>(name.c_str()) };
        
        if (parameter)
            parameter->setValue(value);
        else {
            printf_s("set_parameter: Can not find parameter named %s\n", name.c_str());
            throw std::runtime_error("Unknown parameter");
        }  
    },  "Set a parameter by name", py::arg("name"), py::arg("value"));
    Parameterized_py.def("get_parameter", [] (Parameterized & self, const std::string & name) -> double {
        const auto parameter { self.findParameter<double>(name.c_str()) };
        if (parameter)
            return static_cast<double>(*parameter);  
        else {
            printf_s("get_parameter: Can not find parameter named %s\n", name.c_str());
            throw std::runtime_error("Unknown parameter");
        }
    },  "Get a parameter by name", py::arg("name"));
    
    // Setter/getter overloads for Parameter<std::string>
    Parameterized_py.def("set_parameter", [] (Parameterized & self, const std::string & name, const std::string & value) -> void {
        auto parameter { self.findParameter<std::string>(name.c_str()) };
        
        if (parameter)
            parameter->setValue(value);
        else {
            printf_s("set_parameter: Can not find parameter named %s\n", name.c_str());
            throw std::runtime_error("Unknown parameter");
        }
    },  "Set a parameter by name", py::arg("name"), py::arg("value"));
    Parameterized_py.def("get_parameter", [] (Parameterized & self, const std::string & name) -> std::string {
        const auto parameter { self.findParameter<std::string>(name.c_str()) };
        
        if (parameter)
            return static_cast<std::string>(*parameter);  
        else {
            printf_s("get_parameter: Can not find parameter named %s\n", name.c_str());
            throw std::runtime_error("Unknown parameter");
        }
    },  "Get a parameter by name", py::arg("name"));
    
    Parameterized_py.def("read_parameters", [] (Parameterized & self, const std::string & fileName) -> void {
        StringBuffer pathBuffer;
        FilePathTranslator::translate(pathBuffer, fileName.c_str());
        self.readParameters(pathBuffer.getString());     
    },  "Load parameters from file", py::arg("filename"));
    Parameterized_py.def("write_parameters", [] (Parameterized & self, const std::string & fileName) -> void {
        StringBuffer pathBuffer;
        FilePathTranslator::translate(pathBuffer, fileName.c_str());
        self.writeParameters(pathBuffer.getString());     
    },  "Load parameters from file", py::arg("filename"));
    
    BIND_CLASS(VoltageSource, VoltageSource, Parameterized)
    VoltageSource_py.def("get_voltage", &VoltageSource::getVoltage, "Returns the voltage in Volt at time t", py::arg("t"));
    
    BIND_CLASS(Model, Model, Parameterized)
    
    BIND_CLASS(ModelOutput, ModelOutput)
    
    ModelOutput_py.def(
        "write_results",
        [] (ModelOutput & self, const std::string & fileName, const std::vector<std::string> & resultNames) {
            StringBuffer fileNameBuffer;
            FilePathTranslator::translate(fileNameBuffer, fileName.c_str());
     
            if (resultNames.size() > 0)
                self.writeResults(fileNameBuffer.getString(), resultNames);
            else
                self.writeResults(fileNameBuffer.getString());
        },
        "Write results to file",
        py::arg("file_name"),
        py::arg("result_names") = std::vector<std::string>{}
    );
    
    ModelOutput_py.def("get_result", [] (ModelOutput & self, const std::string & name) -> ModelResult * {
        const auto result { self.findResult(name.c_str()) };
        if (result)
            return result;
        else {
            printf_s("get_result: Can not find result named %s\n", name.c_str());
            throw std::runtime_error("Unknown result");
        }
    }, "Get a result by name", py::arg("name"), py::return_value_policy::reference_internal);
      
    // ModelResult (cast to NumPy array using np.array(res, copy = False))
    auto ModelResult_py { py::class_<ModelResult>(m, "ModelResult", py::buffer_protocol()) };
    ModelResult_py.def_buffer(
        [](ModelResult & self) -> py::buffer_info {
            return py::buffer_info(
                self.getData(),                          /* Pointer to buffer */
                sizeof(double),                          /* Size of one scalar */
                py::format_descriptor<double>::format(), /* Python struct-style format descriptor */
                1,                                       /* Number of dimensions */
                { self.getSize() },                      /* Buffer dimensions */
                { sizeof(double) }                       /* Strides in each dimension */
            );
        }
    );
   
    /* === Generated bindings === */
        
    BIND_PARAMETERIZED(VoltageSweep, VoltageSweep, VoltageSource)
    VoltageSweep_py.def(py::init<>());

    BIND_PARAMETERIZED(VoltagePulse, VoltagePulse, VoltageSource)
    VoltagePulse_py.def(py::init<>());
    
    BIND_PARAMETERIZED(VoltageStep, VoltageStep, VoltageSource)
    VoltageStep_py.def(py::init<>());
    
    /* === VCModel === */
     
    BIND_PARAMETERIZED(VCModel, UsingOperator::VCModel, Model)
    VCModel_py.def(py::init<>());
    
    BIND_CLASS(VCModelOutput, VCModelOutput, ModelOutput);
    bindModelResults(VCModelOutput_py);
    // no constructor 
    
    /*
    What Python sees as Simulation class is in reality only a pointer to
    SimulationInterface which defines the only public function run().
    Using this strategy, the existing Python interface stays unchanged.
    */
    BIND_PARAMETERIZED(Simulation, SimulationInterface, Parameterized)
    Simulation_py.def(
        py::init(
            [] (UsingOperator::VCModel * model, const std::size_t maxOutputSize) {
                return new Simulation<UsingOperator::VCModel>(model, maxOutputSize);
            }
        ),
        "Constructor", py::arg("model"), py::arg("max_size"),
        py::return_value_policy::take_ownership
        
    );
    Simulation_py.def(
        "run", &SimulationInterface::run, "Run the simulation", 
        py::arg("voltage_source"),
        py::arg("reset_state") = true,
        py::return_value_policy::reference_internal
    );
}