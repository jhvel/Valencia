#ifndef BINDING_H
#define BINDING_H

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "Parameter.hpp"
#include "Model.hpp"

#define BIND_PARAMETER(pyClass, class, type, parameter, index) \
pyClass.def_property( \
    parameter->name, \
    [index] (const class & self) -> type { \
        return ((Parameterized *) &self)->getParameter<type>(index)->value; \
    }, \
    [index] (class & self, const type & value) -> void { \
        ((Parameterized *) &self)->getParameter<type>(index)->value = value; \
    }, \
    parameter->description \
);

#ifdef WINDOWS
    #include <direct.h>
    #define GET_CURRENT_DIR _getcwd
#else
    #include <unistd.h>
    #define GET_CURRENT_DIR getcwd
#endif

namespace py = pybind11;

template<typename T, typename... B>
void bindParameters(py::class_<T, B...> & pyClass, const char * pyName) {
    using Parameterized = Valencia::Parameterized;
    
    static_assert(std::is_base_of<Parameterized, T>::value, "T not derived from Parameterized");
    
    T instance;
    
    const auto parameters { instance.getParameters() };
    const auto count { instance.getParameterCount() };
    
    for (int i = 0; i < count; i++) {
        auto parameter { parameters[i] };

        // Add parameter types here
        if (HAS_TYPE(parameter, int))
            BIND_PARAMETER(pyClass, T, int, parameter, i)
        else if (HAS_TYPE(parameter, double))
            BIND_PARAMETER(pyClass, T, double, parameter, i)
        else if (HAS_TYPE(parameter, std::string))
            BIND_PARAMETER(pyClass, T, std::string, parameter, i)
        else 
            throw std::runtime_error("Parameter has unregistered type. Please register the type in python/Binding.h");
    }
    
    // Print parameters in description
    pyClass.def("__repr__",
        [pyName](T & self) -> const char * {
            const auto parameters { self.getParameters() };
            const auto count { self.getParameterCount() };
            Valencia::StringBuffer description;
            
            // Header
            description.putStringFormatted("<valencia.%s\n", pyName);
            
            // Align parameter values to improve readability
            std::size_t nameSizes[count] {};
            std::size_t maxNameSize { 0 };
            
            for (int i = 0; i < count; i++) {
                nameSizes[i] = std::strlen(parameters[i]->name);
                if (nameSizes[i] > maxNameSize)
                    maxNameSize = nameSizes[i];
            }
           
            // Parameters
            for (int i = 0; i < count; i++) {
                description.put("  ");
                description.put(parameters[i]->name);
                description.put(' ');
                description.putCharRepeated('.', maxNameSize - nameSizes[i] + 2);
                description.put(' ');
                parameters[i]->writeValue(description);
                
                const Valencia::Unit * unit { parameters[i]->unit };
                if (unit)
                    description.putStringFormatted(" %s", unit->symbol);
                description.put('\n');
            }
            
            // Footer
            description.put('>');
            
            char * buffer = new char[description.getStringSize() + 1];
            std::strcpy(buffer, description.getString());
            return buffer;
        },
        py::return_value_policy::take_ownership /* Python needs to free buffer for us */
    );
}

template<typename T, typename... B>
void bindModelResults(py::class_<T, B...> & pyClass) {
    static_assert(std::is_base_of<Valencia::ModelOutput, T>::value, "T not derived from ModelOutput");
    
    T instance;
    
    const auto results { instance.getResults() };
    const auto count { instance.getResultCount() };
    
    for (int i = 0; i < count; i++) {
        pyClass.def_property_readonly(
            results[i]->name,
            [i] (T & self) -> Valencia::ModelResult * {
                return ((Valencia::ModelOutput *) &self)->getResults()[i];
            }
        );
    }
}

// Barebones implementation, Windows specific!
// TODO: Make platform-independent, detect absolute/relative path
namespace FilePathTranslator {
    
    void translate(Valencia::StringBuffer & destination, const char * fileName) {
        Valencia::StringBuffer fileNameBuffer;
        fileNameBuffer.put(fileName);
        fileNameBuffer.replaceAll('/', '\\');
        
        char currentDir[FILENAME_MAX];
        GET_CURRENT_DIR(currentDir, FILENAME_MAX);
        
        destination.putString(currentDir);
        destination.trim('\\');
        
        if (!fileNameBuffer.startsWith("\\"))
            destination.put('\\');
        
        destination.putBuffer(fileNameBuffer); // .put() crashes?!
    }
};

#endif